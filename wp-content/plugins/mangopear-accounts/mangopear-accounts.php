<?php

	/**
	 * Plugin name:		Mangopear accounts
	 * Plugin URI:		http://mangopear.co.uk
	 * Description:		Add an accounts section to the website
	 * Version:			1.0.0
	 * Author:			Andi North
	 * License:			GPL
	 * Copyright:		Andi North, Mangopear creative
	 */
	




	/**
	 * Enqueue styles and scripts
	 */
	
	function mpaccounts__enqueue() {
		wp_enqueue_style('mpaccounts-style', plugin_dir_path(__FILE__) . 'admin/resources/css/compiled/screen.css');
	}


	add_action('wp_enqueue_scripts','mpaccounts__enqueue');
	




	/**
	 * Add admin page
	 */

	add_action( 'admin_menu', 'mpaccounts_add_admin_page' );


	function mpaccounts_add_admin_page(){
		add_menu_page('Accounts','Accounts', 'manage_options', 'mangopear-accounts', 'mpaccounts__admin_page_content', '', 6);
		add_submenu_page('mangopear-accounts', 'Contracts', 'Contracts', 'manage_options', 'edit.php?post_type=accounts__contracts');

		include(plugin_dir_path(__FILE__) . 'admin/admin.php');
	}





	/**
	 * Register custom post type:: Accounts forms
	 */
	
	if ( ! function_exists('mpaccounts__register_cpt__forms') ) {
		function mpaccounts__register_cpt__forms() {
			$labels = array(
				'name'                => _x('Contracts', 'Post Type General Name', 'mpaccounts'),
				'singular_name'       => _x('Contract', 'Post Type Singular Name', 'mpaccounts'),
				'menu_name'           => __('Contracts', 'mpaccounts'),
				'parent_item_colon'   => __('Parent contract:', 'mpaccounts'),
				'all_items'           => __('All contracts', 'mpaccounts'),
				'view_item'           => __('View contract', 'mpaccounts'),
				'add_new_item'        => __('Add new contract', 'mpaccounts'),
				'add_new'             => __('Add new', 'mpaccounts'),
				'edit_item'           => __('Edit contract', 'mpaccounts'),
				'update_item'         => __('Update contract', 'mpaccounts'),
				'search_items'        => __('Search contracts', 'mpaccounts'),
				'not_found'           => __('Not found', 'mpaccounts'),
				'not_found_in_trash'  => __('Not found in trash', 'mpaccounts'),
			);
			$rewrite = array(
				'slug'                => 'my-account/legal/contracts',
				'with_front'          => true,
				'pages'               => true,
				'feeds'               => true,
			);
			$args = array(
				'label'               => __('mpaccounts', 'mpaccounts'),
				'labels'              => $labels,
				'supports'            => array('title', 'editor', 'comments', 'revisions', 'page-attributes'),
				'taxonomies'          => array('totton__faq__categories'),
				'hierarchical'        => true,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => false,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 49,
				'can_export'          => true,
				'has_archive'         => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'rewrite'             => $rewrite,
				'capability_type'     => 'page',
			);
			register_post_type('accounts__contracts', $args);
		}

		add_action('init', 'mpaccounts__register_cpt__forms', 0);
	}