# Hildon: Core website functionality (Plugin: hildon-core)

Version: 1.0.0

## Author:

Andi North ([@mangopearuk](https://twitter.com/MangopearUK) / [mangopear.co.uk](https://mangopear.co.uk)) on hehalf of Hildon Limited.

## Summary

This is the Hildon website (http://hildon.com) core functionality plugin source code.

This plugin should always be part of the Hildon website repository and project.

## Usage

This plugin only contains PHP code, so there's no code concatenation or minification. Simply install this plugin into your WordPress installation and you're ready to go.

This plugin contains several functions that you can reference in your theme or plugin. The available [functions are documented](https://mangopear.co.uk/account/clients/hildon/docs/plugins/hildon-core/).