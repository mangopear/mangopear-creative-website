<?php

	/**
	 * Register custom post type for the website: Resources [resources]
	 *
	 * Resources could be simple links through to other websites or more 
	 * detailed write ups on certain techniques or tools.
	 *
	 * @category 	Post types
	 * @package  	mangopear
	 * @author  	Andi North <andi@mangopear.co.uk>
	 * @copyright  	2015 Mangopear creative
	 * @license   	GNU General Public License <http://opensource.org/licenses/gpl-license.php>
	 * @version  	1.0.0
	 * @link 		https://mangopear.co.uk/
	 * @since   	1.0.0
	 */
	

	/**
	 * Contents
	 *
	 * [1]	Define the labels for our post type
	 * [2]	Define the permalinks for the post type
	 * [3]	Define settings for the post type
	 * [4]	Register the post type in WordPress
	 * [5]	Hook into plugin activation
	 */
	

	if (!function_exists('mangopear_register_type_retainer')) {
		function mangopear_register_type_retainer() {
			
			/**
			 * [1]	Define the labels for our post type
			 */
			
			$labels = array(
				'name'					=> _x('Retainer',	'Post Type General Name', 	'mangopear'),
				'singular_name'			=> _x('Retainer',			'Post Type Singular Name',	'mangopear'),
				'menu_name'				=> __('Retainer',			'mangopear'),
				'parent_item_colon'		=> __('Parent retainer:',	'mangopear'),
				'all_items'				=> __('All retainers',		'mangopear'),
				'view_item'				=> __('View retainer',		'mangopear'),
				'add_new_item'			=> __('Add new retainer',	'mangopear'),
				'add_new'				=> __('Add new',			'mangopear'),
				'edit_item'				=> __('Edit retainer',		'mangopear'),
				'update_item'			=> __('Update retainer',	'mangopear'),
				'search_items'			=> __('Search retainers',	'mangopear'),
				'not_found'				=> __('Not found',			'mangopear'),
				'not_found_in_trash'	=> __('Not found in trash',	'mangopear'),
			);


			/**
			 * [2]	Define the permalinks for the post type
			 */
			
			$rewrite = array(
				'slug'					=> 'account/finance/retainer',
				'with_front'			=> true,
				'pages'					=> false,
				'feeds'					=> false,
			);


			/**
			 * [3]	Define settings for the post type
			 */
			
			$args = array(
				'label'					=> __('Retainer', 'mangopear'),
				'labels'				=> $labels,
				'supports'				=> array('title', 'editor'),
				'taxonomies'			=> array('retainer__client', 'retainer__cat'),
				'hierarchical'			=> false,
				'public'				=> true,
				'show_ui'				=> true,
				'show_in_menu'			=> true,
				'menu_icon'				=> 'dashicons-welcome-learn-more',
				'show_in_nav_menus'		=> false,
				'show_in_admin_bar'		=> true,
				'menu_position'			=> 48,
				'can_export'			=> true,
				'has_archive'			=> true,
				'exclude_from_search'	=> true,
				'publicly_queryable'	=> true,
				'rewrite'				=> $rewrite,
				'capability_type'		=> 'post',
			);


			/**
			 * [4]	Register the post type in WordPress
			 */
			
			register_post_type('retainer', $args);
		}


		/**
		 * [5]	Hook into plugin activation
		 */
		
		add_action('init', 'mangopear_register_type_retainer', 0);
	}





	/**
	 * Custom taxonomy for resource types
	 */
	
	if (! function_exists('mangopear_register_taxonomy_retainer_client')) {
		function mangopear_register_taxonomy_retainer_client() {
			$labels = array(
				'name'                       => _x('Client', 								'Taxonomy General Name', 	'mangopear'),
				'singular_name'              => _x('Client', 								'Taxonomy Singular Name', 	'mangopear'),
				'menu_name'                  => __('Client', 								'mangopear'),
				'all_items'                  => __('All categories', 						'mangopear'),
				'parent_item'                => __('Parent category', 						'mangopear'),
				'parent_item_colon'          => __('Parent category:', 						'mangopear'),
				'new_item_name'              => __('New category', 							'mangopear'),
				'add_new_item'               => __('Add new category', 						'mangopear'),
				'edit_item'                  => __('Edit category', 						'mangopear'),
				'update_item'                => __('Update category', 						'mangopear'),
				'separate_items_with_commas' => __('Separate categories with commas', 		'mangopear'),
				'search_items'               => __('Search categories', 					'mangopear'),
				'add_or_remove_items'        => __('Add or remove categories', 				'mangopear'),
				'choose_from_most_used'      => __('Choose from the most used categories', 	'mangopear'),
				'not_found'                  => __('Category not found', 					'mangopear'),
			);


			$rewrite = array(
				'slug'                       => 'account/finance/retainer/client',
				'with_front'                 => true,
				'hierarchical'               => false,
			);


			$args = array(
				'labels'                     => $labels,
				'hierarchical'               => false,
				'public'                     => true,
				'show_ui'                    => true,
				'show_admin_column'          => true,
				'show_in_nav_menus'          => false,
				'show_tagcloud'              => false,
				'query_var'                  => 'retainer__client',
				'rewrite'                    => $rewrite,
			);


			register_taxonomy('retainer__client', array('retainer'), $args);
		}


		add_action('init', 'mangopear_register_taxonomy_retainer_client', 0);
	}





	/**
	 * Custom taxonomy for resource tags
	 */
	
	if (! function_exists('mangopear_register_taxonomy_retainer_cat')) {
		function mangopear_register_taxonomy_retainer_cat() {
			$labels = array(
				'name'                       => _x('Category', 								'Taxonomy General Name', 	'mangopear'),
				'singular_name'              => _x('Category', 								'Taxonomy Singular Name', 	'mangopear'),
				'menu_name'                  => __('Category', 								'mangopear'),
				'all_items'                  => __('All categories', 						'mangopear'),
				'parent_item'                => __('Parent category', 						'mangopear'),
				'parent_item_colon'          => __('Parent category:', 						'mangopear'),
				'new_item_name'              => __('New category', 							'mangopear'),
				'add_new_item'               => __('Add new category', 						'mangopear'),
				'edit_item'                  => __('Edit category', 						'mangopear'),
				'update_item'                => __('Update category', 						'mangopear'),
				'separate_items_with_commas' => __('Separate categories with commas', 		'mangopear'),
				'search_items'               => __('Search categories', 					'mangopear'),
				'add_or_remove_items'        => __('Add or remove categories', 				'mangopear'),
				'choose_from_most_used'      => __('Choose from the most used categories', 	'mangopear'),
				'not_found'                  => __('Category not found', 					'mangopear'),
			);


			$rewrite = array(
				'slug'                       => 'account/finance/retainer/category',
				'with_front'                 => true,
				'hierarchical'               => false,
			);


			$args = array(
				'labels'                     => $labels,
				'hierarchical'               => false,
				'public'                     => true,
				'show_ui'                    => true,
				'show_admin_column'          => true,
				'show_in_nav_menus'          => false,
				'show_tagcloud'              => false,
				'query_var'                  => 'retainer__cat',
				'rewrite'                    => $rewrite,
			);


			register_taxonomy('retainer__cat', array('retainer'), $args);
		}


		add_action('init', 'mangopear_register_taxonomy_retainer_cat', 0);
	}











	/**
	 * Add user meta field
	 */
	
	add_action( 'show_user_profile', 'mangopear_add_retainer_client_id' );
	add_action( 'edit_user_profile', 'mangopear_add_retainer_client_id' );

	function mangopear_add_retainer_client_id($user) {
		?>
			<h3>Custom Mangopear fields</h3>

			<table class="form-table">
				<tr>
					<th><label for="retainer_client_id">Client ID (for retainer)</label></th>
					<td><input type="text" name="retainer_client_id" value="<?php echo esc_attr(get_the_author_meta('retainer_client_id', $user->ID)); ?>" class="regular-text" /></td>
				</tr>
			</table>
		<?php
	}


	add_action( 'personal_options_update', 'mangopear_save_retainer_client_id' );
	add_action( 'edit_user_profile_update', 'mangopear_save_retainer_client_id' );

	function mangopear_save_retainer_client_id($user_id) { update_user_meta($user_id,'retainer_client_id', sanitize_text_field($_POST['retainer_client_id'])); }
	
?>