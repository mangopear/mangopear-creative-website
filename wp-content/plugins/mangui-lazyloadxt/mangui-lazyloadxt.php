<?php


	/**
	 * Plugin name: MangUI :: LazyLoadXT
	 * Plugin URI:	https://mangopear.co.uk/
	 * Description:	This plugin adds LazyLoadXT functionality to your website. <strong>Must be used in conjunction with the MangUI framework.</strong>
	 * Version:		1.0.0
	 * Author:		Andi North (from Mangopear creative)
	 * Author URI:	https://mangopear.co.uk
	 * License:		GNU General Public License
	 */


	/**
	 * [MangUI] Add lazy loading functionality
	 *
	 * This function will replace all images added through the editor or through featured images
	 * with lazy loading images.
	 *
	 * Can also be extended to images not covered in this functionality.
	 *
	 * @category 	Performance
	 * @package  	MangUI
	 * @author  	Andi North <andi@mangopear.co.uk>
	 * @copyright  	2015 Mangopear creative
	 * @license   	GNU General Public License <http://opensource.org/licenses/gpl-license.php>
	 * @version  	1.0.0
	 * @since   	1.0.0
	 */
	

	if (! defined('ABSPATH')) { exit; } // Kill the request if this page is accessed directly.





	class ManguiLazyLoadXT {

		/**
		 * Set up our variables
		 */
		
		protected $directory; 						// Plugin directory
		protected $lazyloadxt_version = '1.0.6'; 	// Version of the script **NOT THE PLUGIN VERSION**
		protected $settingsClass;					// Settings class for admin area
		protected $settings;						// Settings for this plugin





		/**
		 * Construct
		 */
		
		function __construct() {
			if (is_admin()) : // If we're in the admin area, load the settings class
				require 'settings.php';
				$settingsClass = new ManguiLazyLoadXTSettings;

				// If this is the first time we've enabled the plugin, setup default settings
				register_activation_hook(__FILE__, array($settingsClass, 'first_time_activation'));
				add_filter('plugin_action_links' . plugin_basename(__FILE__), array($settingsClass, 'mangui_lazyloadxt_action_links'));


			else :
				$this->settings = $this->get_settings(); // Store our settings in memory to reduce MySQL calls
				$this->directory = plugin_dir_url(__FILE__);
				if ($this->settings['cdn']) { $this->$lazyloadxt_version = '1.0.5'; }


				add_filter('the_content', array($this, 'filter_html')); 												 // Replace attrs in the_content
				if ($this->settings['text_widgets']) { add_filter('widget_text', array($this, 'filter_html')); } 		 // Replace attrs in text widgets
				if ($this->settings['thumbnails'])   { add_filter('post_thumbnail_html', array($this, 'filter_html')); } // Replace attrs in post thumbnail
				if ($this->settings['avatars'])      { add_filter('get_avatar', array($this, 'filter_html')); }			 // Replace attrs in avatars


			endif;
		}





		/**
		 * Get the settings for our plugin
		 */
		
		function get_settings() {
			$general =  get_option('mangui_lazyloadxt_general');  // Get settings from DB
			$effects =  get_option('mangui_lazyloadxt_effects');  // Get settings from DB
			$addons =   get_option('mangui_lazyloadxt_addons');   // Get settings from DB
			$advanced = get_option('mangui_lazyloadxt_advanced'); // Get settings from DB


			// Set the array of options
			$settings_array = array(
				'minimize_scripts',
				'cdn',
				'footer',
				'load_extras',
				'thumbnails',
				'avatars',
				'textwidgets',
				'ajax',
				'excludeclasses',
				'fade_in',
				'spinner',
				'script_based_tagging',
				'responsive_images',
				'print',
				'background_image',
				'deferred_load',
			);


			$settings = array(); // Start a fresh


			// Loop through the settings we're looking for and set them if they exist...
			foreach ($settings_array as $setting) :
				if     ($general && array_key_exists('mangui_lazyloadxt_' . $setting, $general)) : $return = $general['mangui_lazyloadxt_' . $setting];
				elseif ($effects && array_key_exists('mangui_lazyloadxt_' . $setting, $effects)) : $return = $effects['mangui_lazyloadxt_' . $setting];
				elseif ($addons  && array_key_exists('mangui_lazyloadxt_' . $setting, $addons))  : $return =  $addons['mangui_lazyloadxt_' . $setting];
				else : $return = false;
				endif;

				$settings[$setting] = $return;
			endforeach;


			// If enabled, set the advanced settings to an array
			if ($advanced['mangui_lazyloadxt_enabled']) :
				foreach ($advanced as $key => $val) :
					if ($key != 'mangui_lazyloadxt_enabled') :
						$settings['advanced'][str_replace('mangui_lazyloadxt_', '', $key)] = $val;
					endif;
				endforeach;

			else :
				$settings['advanced'] = false;

			endif;


			$settings['excludeclasses'] = ($settings['excludeclasses']) ? explode(' ',$settings['excludeclasses']) : array();


			return $settings; // Return our settings
		}





		/**
		 * Filter our HTML
		 */
		
		function filter_html($content) {
			if (is_feed()) { return $content; } // We don't want to filter the HTML on feeds


			if (strlen($content)) : // If there's anything there, replace the 'src' with 'data-src'
				$newcontent = $content;
				$newcontent = $this->preg_replace_html($newcontent, array('img')); // Replace 'src' with 'data-src' on images
				if ($this->settings['load_extras']) { $newcontent = $this->preg_replace_html($newcontent, array('iframe', 'embed', 'video', 'audio')); }
				return $newcontent;

			else :
				return $content;

			endif;
		}





		/**
		 * Replace HTML segements
		 */
		
		function preg_replace_html($content, $tags) {
			$search = array(); $replace = array(); // Set up empty arrays to be populated later
			$attrs = implode('|', array('src', 'poster')); // Attributes to search for
			$src_req = array('img', 'video'); // Elements that require a 'src' attr to be valid HTML


			/**
			 * Loop through tags
			 */
			
			foreach ($tags as $tag) :
				$self_closing = in_array($tag, array('img', 'embed', 'source')); // Is the tag self closing?
				$tag_end = ($self_closing) ? '\/' : '<\/' . $tag;


				preg_match_all('/<'.$tag.'[\s\r\n]([^<]+)('.$tag_end.'>)(?!<noscript>|<\/noscript>)/is', $content, $matches);


				// If the tag exists, loop through and replace
				if (count($matches[0])) :
					foreach($matches[0] as $match) :
						preg_match('/[\s\r\n]class=[\'"](.*?)[\'"]/', $match, $classes);
						$classes_r = (array_key_exists(1, $classes)) ? explode('  ', $classes[1]) : array(); // If it has assigned classes, explode them#


						// But first check that the tag doesn't have any exluded classes
						if (count(array_intersect($classes_r, $this->settings['excludeclasses'])) == 0) :
							$original = match; // Set the original version for the <noscript>
							array_push($search, $original); // And add it to the search array


							if ($this->settings['script_based_tagging']) :
								if ($self_closing) : $replace_markup = '<script>L();</script>' . $original; // If it's self-closing use L();
								else 			   : $replace_markup = '<script>Lb(\'' . $tag . '\');</script>' . $original . '<script>Le();</script>'; // Otherwise, use Lb(); and Le();
								endif;
								array_push($replace, $replace_markup); // Add it to the $replace array


							else :
								$src = (in_array($tag, $src_req)) ? ' src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"' : ''; // If src attr is required, set a default
								$src = ($tag == 'audio') ? ' src="' . $this->directory . 'assets/empty.mp3' : $src; // If it's an audio tag, set the src to a blank MP3


								$replace_markup  = $match; // Set replace HTML
								$replace_markup  = preg_replace('/[\s\r\n](' . $attrs . ')?=/', $src . ' data-$1=', $replace_markup); // Now replace attr with data-attr
								$replace_markup .= '<noscript>' . $original . '</noscript>'; // And add the original in as <noscript>
								array_push($replace, $replace_markup); // And add it to the $replce array
							endif;
						endif;
					endforeach;
				endif;
			endforeach;


			$newcontent = str_replace($search, $replace, $content);
			return $newcontent;
		}
	}





	$mangui_lazyloadxt = new ManguiLazyLoadXT();


	function get_lazyloadxt_html($html = '') {
		global $mangui_lazyloadxt;
		return $mangui_lazyloadxt->filter_html($html);
	}