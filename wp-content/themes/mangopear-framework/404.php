<?php
	
	/**
	 * Get the header code
	 */
	get_header();

?>





	<div class="typography--standard">
		<h1 class="header--stylised">Error 404!</h1>
		<h2>Looks like there's an issue...</h2>
		<p>I'd suggest checking the URL of the page you're trying to access, make sure the URL is spelt correctly.</p>
		<p>It would be great if you could let me know about the issue on <a href="//twitter.com/MangopearUK">Twitter</a> or by using my <a href="/contact/">contact form</a></p>
		

		<p style="margin-bottom: 60px;">
			<a href="/" class="button--secondary">
				Return to the homepage
				<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
					<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
				</svg>
			</a>
		</p>
	</div><!-- /.container -->





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>