		<footer class="panel  panel--primary  panel--hire-me">
			<div class="container">
				<div class="grid">
					<div class="grid__item  three-quarters  palm--one-whole  hire-me__content">
						<h2 class="hire-me__heading">So you're interested in working with me?</h2>
						<p class="hire-me__text">Simply get in touch with me and we can arrange to meet up or have a Skype chat to discuss your requirements.</p>
					</div><!-- /.grid__item -->


					<div class="grid__item  one-quarter  palm--one-whole  hire-me__call-to-action">
						<a href="/contact/hire/" class="button--primary  hire-me__button">
							<span class="button__text">Get in touch</span>
							<svg class="button__icon--right  icon--chevron-right" viewBox="0 0 16 16" width="20" height="20">
								<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
							</svg>
						</a><!-- /.hire-me__button -->
					</div><!-- /.grid__item -->
				</div><!-- /.grid -->
			</div><!-- /.container -->
		</footer><!-- /.panel -->





		<footer class="panel  panel--secondary  panel--portfolio">
			<div class="container">
				<h2 class="panel__heading  panel__heading--portfolio">Portfolio &amp; case studies</h2>
				

				<div class="grid">
					<div class="grid__item  one-third  lap--one-half  palm--one-whole">
						<article class="clearfix  portfolio-item  portfolio-item--jeakins  portfolio-item--has-link">
							<div class="portfolio-item__header">
								<h3 class="portfolio-item__heading"><a href="/portfolio/jeakins-coach-travel/" class="portfolio-item__header__link">Jeakins Coach Travel</a></h3>
								<p class="portfolio-item__content">A family-run coach operator based in the heart of Surrey.</p>
							</div>
							
							<a href="/portfolio/jeakins-coach-travel/" class="button--secondary  portfolio-item__button">
								<span class="button__text">Read more</span>
								<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
									<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
								</svg>
							</a>
						</article>
					</div><!-- /.grid__item -->


					<div class="grid__item  one-third  lap--one-half  palm--one-whole">
						<article class="clearfix  portfolio-item  portfolio-item--gardbus  portfolio-item--has-link">
							<div class="portfolio-item__header">
								<h3 class="portfolio-item__heading"><a href="/portfolio/gardbus/" class="portfolio-item__header__link">Gardbus</a></h3>
								<p class="portfolio-item__content">A friendly, local operator running bus services in and around Ringwood.</p>
							</div>
							
							<a href="/portfolio/gardbus/" class="button--secondary  portfolio-item__button">
								<span class="button__text">Read more</span>
								<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
									<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
								</svg>
							</a>
						</article>
					</div><!-- /.grid__item -->


					<div class="grid__item  one-third  lap--one-half  palm--one-whole">
						<article class="clearfix  portfolio-item  portfolio-item--silk  portfolio-item--has-link">
							<div class="portfolio-item__header">
								<h3 class="portfolio-item__heading"><a href="/portfolio/silk-web-toolkit/" class="portfolio-item__header__link">Silk Web Toolkit</a></h3>
								<p class="portfolio-item__content">An open-source, pure HTML5 CMS built for security and performance.</p>
							</div>
							
							<a href="/portfolio/silk-web-toolkit/" class="button--secondary  portfolio-item__button">
								<span class="button__text">Read more</span>
								<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
									<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
								</svg>
							</a>
						</article>
					</div><!-- /.grid__item -->
				</div><!-- /.grid -->


				<a href="/portfolio/" class="button--secondary  button--case-studies">
					<span class="button__text">Read my case studies</span>
					<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
						<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
					</svg>
				</a>
			</div><!-- /.container -->
		</footer><!-- /.panel -->





		<footer class="panel  panel--primary  panel--contact">
			<div class="container">
				<div class="grid">
					<div class="grid__item  one-third">
						<h2 class="contact__heading">Get in touch with me:</h2>
					</div><!-- /.grid__item -->


					<div class="grid__item  one-third">
						<dl class="contact__details">
							<dt class="details__title">Give me a call on:</dt>
							<dd class="details__content  details__content--phone">07415 388 501</dd>


							<dt class="details__title">Email me using:</dt>
							<dd class="details__content  details__content--email"><a href="mailto:hello@mangopear.co.uk" class="">hello@mangopear.co.uk</a></dd>
						</dl>
					</div><!-- /.grid__item -->


					<div class="grid__item  one-third">
						<dl class="contact__details  contact__details--address">
							<dt class="details__title">Write to me at:</dt>
							<dd class="details__content  details__content--address">
								<p>Andi North,<br/> 10 Normans, <br/>Norman Road, <br/>Hampshire, <br/>United Kingdom, <br/>SO23 9PP</p>
							</dd>
						</dl>
					</div><!-- /.grid__item -->
				</div><!-- /.grid -->
			</div><!-- /.container -->
		</footer><!-- /.panel -->





		<footer class="panel  panel--copyright">
			<div class="container">
				<div class="grid">
					<div class="grid__item  one-half  palm--one-whole">
						<nav class="nav  nav--row  nav--footer">
							<ul class="nav__list  nav__list--footer">
								<li class="nav__item  nav__item--footer"><a href="/legal-stuff/terms-conditions/" class="nav__link  nav__link--footer">Terms &amp; conditions</a></li>
								<li class="nav__item  nav__item--footer"><a href="/legal-stuff/privacy-policy/" class="nav__link  nav__link--footer">Privacy Policy</a></li>
								<li class="nav__item  nav__item--footer"><a href="/legal-stuff/cookie-policy/" class="nav__link  nav__link--footer">Cookie Policy</a></li>
							</ul>
						</nav><!-- /.nav -->
					</div><!-- /.grid__item -->


					<div class="grid__item  one-half  palm--one-whole">
						<p class="copyright__content">&copy; 2015: Andi North. All rights reserved.</p>
					</div><!-- /.grid__item -->
				</div><!-- /.grid -->
			</div><!-- /.container -->
		</footer><!-- /.panel -->





		<?php

			/**
			 * Call the WordPress footer function
			 */
			
			wp_footer();

		?>





		<!-- Search form to be revealed in the header -->
		<section class="header__search" data--js--toggle="header__search" style="display: none;">
			<div class="container">
				<button class="js--close-search  button--reset  button--close-search">
					<svg class="icon--close" viewBox="0 0 100 100" width="45">
						<path class="close__cross" d="M51.311,51.315L71.426,31.13c0.361-0.363,0.361-0.951-0.004-1.313c-0.363-0.359-0.951-0.363-1.313,0.004  L50,50L29.891,29.821c-0.363-0.366-0.95-0.363-1.313-0.004c-0.365,0.363-0.365,0.95-0.004,1.313l20.115,20.185L28.574,71.501  c-0.361,0.363-0.361,0.951,0.004,1.313c0.181,0.18,0.417,0.27,0.655,0.27c0.238,0,0.477-0.091,0.658-0.274L50,52.631L70.109,72.81  c0.181,0.183,0.421,0.274,0.658,0.274c0.238,0,0.473-0.091,0.655-0.27c0.365-0.363,0.365-0.95,0.004-1.313L51.311,51.315z"/>
					</svg>
				</button>
				

				<form role="search" method="get" action="<?php bloginfo('url');?>" class="search__form">
					<input type="hidden" value="Search">
					<input type="text" name="s" value="" class="search__input" placeholder="Search this website" tabindex="3">

					<button class="search__submit  button--reset">
						<svg class="icon--search" viewBox="0 0 32 32" width="32">
							<path class="search__glass" d="M30.2,26.1l-5.4-5.4c1.5-2.1,2.3-4.7,2.3-7.5C27.1,6,21.2,0.1,14,0.1C6.7,0.1,0.8,6,0.8,13.2c0,7.3,5.9,13.2,13.1,13.2c2,0,3.9-0.4,5.6-1.2l5.8,5.8c0.6,0.6,1.5,1,2.4,1c0.9,0,1.8-0.4,2.4-1C31.5,29.6,31.5,27.4,30.2,26.1zM6.1,13.2c0-4.4,3.5-7.9,7.9-7.9c4.4,0,7.9,3.5,7.9,7.9c0,4.4-3.5,7.9-7.9,7.9C9.6,21.1,6.1,17.6,6.1,13.2z"/>
						</svg>
					</button>
				</form>
			</div>
		</section><!-- /.header__search -->





		<!-- Modal: Search form -->
		<div class="modal  modal--search">
			<div class="modal__background"></div><!-- /.modal__background -->


			<div class="modal__content">
				<header class="modal__header">
					
				</header>
			</div><!-- /.modal__content -->
		</div><!-- /.modal -->





		<!-- Google Analytics tracking code -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-45542791-1', 'auto');
			ga('send', 'pageview');
		</script>
	</body>
</html>