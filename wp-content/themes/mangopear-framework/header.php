<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php the_title(); ?> | <?php bloginfo('name'); ?></title>
		

		<!-- Make this site responsive, dude -->
		<meta name="viewport" content="width=device-width, initial-scale=1">


		<!-- Google fonts -->
		<link href="http://fonts.googleapis.com/css?family=Source+Code+Pro|Roboto:300,400,700" rel="stylesheet" type="text/css">


		<!-- Change toolbar colour in Chrome -->
		<meta name="theme-color" content="#007b92">


		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('stylesheet_directory'); ?>/resources/images/favicon/favicon.ico">
		<link rel="apple-touch-icon"                  href="<?php bloginfo('stylesheet_directory'); ?>/resources/images/favicon/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="57x57"    href="<?php bloginfo('stylesheet_directory'); ?>/resources/images/favicon/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="72x72"    href="<?php bloginfo('stylesheet_directory'); ?>/resources/images/favicon/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76"    href="<?php bloginfo('stylesheet_directory'); ?>/resources/images/favicon/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114"  href="<?php bloginfo('stylesheet_directory'); ?>/resources/images/favicon/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120"  href="<?php bloginfo('stylesheet_directory'); ?>/resources/images/favicon/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144"  href="<?php bloginfo('stylesheet_directory'); ?>/resources/images/favicon/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152"  href="<?php bloginfo('stylesheet_directory'); ?>/resources/images/favicon/apple-touch-icon-152x152.png">
		

		<?php wp_head(); ?>
	</head>





	<body <?php body_class(); ?>>
		<nav class="nav  nav--row  nav--main  clearfix">
			<div class="container">
				<ol class="nav__list">
					<li class="nav__item  nav__item--home">
						<a href="/" class="nav__link  nav__link--logo">
							<svg version="1.1" class="icon--logo" viewBox="0 0 64 70" width="64" height="70">
								<path class="logo__border" fill="#007A93" d="M2.3,48.1V21.9c0-2.5,1.3-4.8,3.5-6L28.5,2.7c2.2-1.2,4.8-1.2,7,0l22.7,13.1c2.2,1.2,3.5,3.5,3.5,6v26.3c0,2.5-1.3,4.8-3.5,6L35.5,67.3c-2.2,1.2-4.8,1.2-7,0L5.8,54.2C3.6,52.9,2.3,50.6,2.3,48.1z"/>
								<path class="logo__background" fill="#004D5D" d="M32,69.8c-1.5,0-3-0.4-4.3-1.1L5,55.5c-2.6-1.5-4.3-4.4-4.3-7.4V21.9c0-3,1.6-5.9,4.3-7.4L27.7,1.3C29,0.6,30.5,0.2,32,0.2c1.5,0,3,0.4,4.3,1.1L59,14.5c2.6,1.5,4.3,4.4,4.3,7.4v26.3c0,3-1.6,5.9-4.3,7.4L36.3,68.7C35,69.4,33.5,69.8,32,69.8z M32,3.4c-0.9,0-1.9,0.2-2.7,0.7L6.6,17.2c-1.7,1-2.7,2.7-2.7,4.7v26.3c0,1.9,1,3.7,2.7,4.7l22.7,13.1c1.6,0.9,3.7,0.9,5.4,0l22.7-13.1c1.7-1,2.7-2.7,2.7-4.7V21.9c0-1.9-1-3.7-2.7-4.7L34.7,4.1C33.9,3.6,32.9,3.4,32,3.4z"/>
								<path class="logo__type" fill="#6DB4C9" d="M48.8,33.2v11.5H42V34.3c0-2.9-1.4-4.2-3.6-4.2c-1,0-2.1,0.5-3.1,1.7c0,0.5,0.1,1,0.1,1.5v11.5h-6.7V34.3c0-2.9-1.4-4.2-3.6-4.2c-1,0-2,0.5-3,1.8v12.8h-6.7V24.6h6.7v1.2c1.4-1.2,2.8-1.8,5-1.8c2.5,0,4.7,1,6.2,2.7c2.1-1.9,4.1-2.7,7.1-2.7C45.2,24,48.8,27.5,48.8,33.2z"/>
							</svg>
							<span class="visually-hidden">Mangopear creative</span>
						</a>
					</li><!-- /.nav__item -->


					<li class="nav__item"><a href="/what-i-do/" class="nav__link">What I do</a></li>
					<li class="nav__item"><a href="/writing/" class="nav__link">Writing</a></li>
					<li class="nav__item"><a href="/portfolio/" class="nav__link">Portfolio</a></li>
					<li class="nav__item"><a href="/contact/" class="nav__link">Contact</a></li>


					<li class="nav__item  nav__item--search">
						<button class="nav__link  nav__link--search">
							<svg version="1.1" class="icon--search" viewBox="0 0 32 32" width="32" height="22">
								<path class="search__glass" fill="#007A93" d="M30.2,26.1l-5.4-5.4c1.5-2.1,2.3-4.7,2.3-7.5C27.1,6,21.2,0.1,14,0.1C6.7,0.1,0.8,6,0.8,13.2c0,7.3,5.9,13.2,13.1,13.2c2,0,3.9-0.4,5.6-1.2l5.8,5.8c0.6,0.6,1.5,1,2.4,1c0.9,0,1.8-0.4,2.4-1C31.5,29.6,31.5,27.4,30.2,26.1zM6.1,13.2c0-4.4,3.5-7.9,7.9-7.9c4.4,0,7.9,3.5,7.9,7.9c0,4.4-3.5,7.9-7.9,7.9C9.6,21.1,6.1,17.6,6.1,13.2z"/>
							</svg>
							<span class="visually-hidden">Search this website</span>
						</button>
					</li><!-- /.nav__item -->


					<li class="nav__item  nav__item--dots">
						<button class="nav__link  nav__link--dots  js-toggle-perspective">
							<svg width="50" height="50" viewBox="5.0 -10.0 100 135.0">
							    <path fill="currentColor" d="m 58.184786,22.824347 a 8.0812206,8.0812206 0 1 1 -16.162441,0 8.0812206,8.0812206 0 1 1 16.162441,0 z"/>
							    <path fill="currentColor" d="m 58.184786,22.824347 a 8.0812206,8.0812206 0 1 1 -16.162441,0 8.0812206,8.0812206 0 1 1 16.162441,0 z" style="transform: translateY(30px);"/>
							    <path fill="currentColor" d="m 58.184786,22.824347 a 8.0812206,8.0812206 0 1 1 -16.162441,0 8.0812206,8.0812206 0 1 1 16.162441,0 z" style="transform: translateY(60px);"/>
							</svg>
							<span class="visually-hidden">Toggle navigation</span>
						</button>
					</li><!-- /.nav__item -->
				</ol><!-- /.nav__list -->
			</div><!-- /.container -->
		</nav><!-- /.nav -->