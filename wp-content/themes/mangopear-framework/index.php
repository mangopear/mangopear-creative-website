<?php
	
	/**
	 * Get the header code
	 */
	
	get_header();

?>





	<main class="main">
		<section class="lister--blog  container">
			<h1 class="header--stylised">My writings</h1>


			<article class="category__item  category__item--latest-musings">
				<i class="category__icon">
					<svg class="icon--notepad" width="200" height="220" viewBox="5.0 -10.0 100.0 135.0" style="margin-top: 10px; margin-left: -5px;">
						<path d="M94.812,53.149h0.004v-0.994h-0.005c-0.011-0.053-0.052-0.104-0.121-0.144l-51.545-29.85     c-0.639-1.052-1.383-1.848-2.169-2.277c-0.868-0.476-1.728-0.489-2.486-0.04c-0.532,0.315-0.979,0.847-1.325,1.549     c-0.617-0.157-1.218-0.074-1.765,0.25c-0.532,0.315-0.979,0.847-1.325,1.548c-0.617-0.157-1.218-0.074-1.764,0.25     c-0.533,0.315-0.98,0.847-1.325,1.548c-0.618-0.157-1.218-0.074-1.765,0.25c-0.533,0.315-0.98,0.847-1.325,1.549     c-0.618-0.158-1.218-0.074-1.765,0.249c-0.533,0.316-0.979,0.848-1.326,1.549c-0.617-0.157-1.217-0.074-1.764,0.249     c-0.533,0.316-0.979,0.848-1.325,1.549c-0.618-0.157-1.218-0.074-1.765,0.25c-0.533,0.315-0.979,0.847-1.325,1.548     c-0.618-0.157-1.218-0.073-1.765,0.25c-0.527,0.313-0.971,0.837-1.315,1.528c-0.592-0.133-1.166-0.044-1.691,0.268     c-0.538,0.319-0.989,0.857-1.336,1.569c-0.644-0.183-1.269-0.105-1.837,0.231c-0.71,0.421-1.269,1.225-1.637,2.305     c-0.639-0.143-1.256-0.043-1.814,0.3c-1.281,0.785-2.046,2.774-2.046,5.321c0,1.923,0.427,3.948,1.202,5.704     c0.792,1.792,1.862,3.102,3.015,3.687c0.44,0.225,0.883,0.336,1.309,0.336c0.482,0,0.944-0.143,1.362-0.426l46.894,27.156     c0.166,0.096,0.438,0.096,0.604-0.002L94.69,59.828c0.072-0.042,0.113-0.096,0.123-0.151h0.002v-0.993H94.81     c-0.013-0.051-0.052-0.101-0.12-0.141l-1.932-1.118v0.462l-33.62,19.716l-49.114-28.44l0.002-1.448l-2.635,1.545     c-0.116,0.068-0.151,0.167-0.104,0.252H7.279c-0.031-0.068-0.063-0.138-0.094-0.206c-0.728-1.646-1.128-3.548-1.128-5.352     c0-2.208,0.624-3.961,1.629-4.577c0.346-0.212,0.722-0.278,1.123-0.199c-0.174,0.774-0.266,1.65-0.266,2.604     c0,0.089,0.001,0.179,0.003,0.269l-1.156,0.678c-0.055,0.032-0.091,0.07-0.109,0.11H7.259v0.992h0.008     c-0.003,0.067,0.039,0.132,0.124,0.182l3.015,1.746l48.561,28.122c0.166,0.097,0.438,0.096,0.604-0.002L92.53,54.563l2.16-1.266     C94.761,53.255,94.801,53.202,94.812,53.149z M9.798,52.567c-0.587-0.299-1.17-0.859-1.695-1.611l3.072,1.778     C10.752,52.873,10.291,52.818,9.798,52.567z M12.988,36.98c0.516,0.349,1.016,0.902,1.469,1.605l-1.864,1.093     C12.616,38.658,12.754,37.739,12.988,36.98z M16.024,35.092c0.553,0.333,1.092,0.905,1.578,1.65L15.6,37.917     C15.62,36.84,15.77,35.876,16.024,35.092z M19.099,33.338c0.536,0.343,1.056,0.907,1.526,1.633l-1.936,1.135     C18.711,35.055,18.854,34.112,19.099,33.338z M22.188,31.542c0.533,0.34,1.052,0.9,1.521,1.623l-1.93,1.13     C21.802,33.25,21.945,32.311,22.188,31.542z M25.278,29.743c0.531,0.34,1.047,0.896,1.515,1.614l-1.924,1.128     C24.893,31.444,25.036,30.51,25.278,29.743z M28.368,27.945c0.529,0.338,1.043,0.892,1.509,1.605l-1.917,1.123     C27.983,29.638,28.127,28.708,28.368,27.945z M31.458,26.146c0.526,0.337,1.038,0.888,1.503,1.597l-1.911,1.12     C31.074,27.833,31.217,26.908,31.458,26.146z M34.548,24.348c0.524,0.335,1.034,0.883,1.497,1.588l-1.905,1.116     C34.166,26.027,34.308,25.106,34.548,24.348z M37.637,22.55c0.523,0.334,1.03,0.878,1.491,1.578l-1.898,1.112     C37.257,24.22,37.398,23.305,37.637,22.55z M38.935,20.595c0.485-0.287,1.031-0.269,1.623,0.055     c0.579,0.317,1.145,0.899,1.654,1.671l-2.329,1.365c-0.575-0.882-1.229-1.558-1.915-1.957     C38.231,21.209,38.557,20.819,38.935,20.595z M35.845,22.394c0.307-0.182,0.637-0.241,0.986-0.18     c-0.253,0.769-0.411,1.672-0.462,2.676c-0.465-0.594-0.969-1.059-1.491-1.362C35.142,23.007,35.467,22.618,35.845,22.394z      M32.755,24.192c0.307-0.182,0.637-0.241,0.986-0.179c-0.253,0.768-0.411,1.671-0.462,2.676     c-0.465-0.595-0.968-1.059-1.491-1.363C32.052,24.805,32.378,24.416,32.755,24.192z M29.666,25.99     c0.305-0.181,0.636-0.24,0.986-0.179c-0.254,0.768-0.412,1.672-0.463,2.676c-0.465-0.595-0.969-1.06-1.491-1.363     C28.962,26.603,29.288,26.213,29.666,25.99z M26.575,27.789c0.307-0.182,0.637-0.241,0.986-0.179     c-0.253,0.768-0.411,1.671-0.462,2.675c-0.464-0.595-0.968-1.059-1.491-1.362C25.872,28.402,26.198,28.012,26.575,27.789z      M23.486,29.586c0.306-0.182,0.636-0.241,0.986-0.179c-0.254,0.768-0.412,1.671-0.462,2.675     c-0.466-0.594-0.969-1.059-1.492-1.362C22.782,30.2,23.108,29.81,23.486,29.586z M20.396,31.384     c0.306-0.181,0.636-0.24,0.986-0.179c-0.254,0.768-0.412,1.672-0.462,2.676c-0.466-0.595-0.969-1.06-1.492-1.363     C19.692,31.998,20.019,31.608,20.396,31.384z M17.307,33.183c0.306-0.182,0.636-0.241,0.986-0.18     c-0.245,0.742-0.401,1.612-0.457,2.575c-0.462-0.57-0.96-1.015-1.477-1.302C16.619,33.776,16.938,33.401,17.307,33.183z      M14.3,34.978c0.283-0.168,0.587-0.23,0.908-0.19c-0.256,0.771-0.416,1.68-0.467,2.69c-0.446-0.571-0.928-1.023-1.429-1.327     C13.58,35.611,13.913,35.208,14.3,34.978z M11.127,36.779c0.328-0.194,0.685-0.249,1.063-0.163     c-0.308,0.94-0.475,2.083-0.475,3.364c0,0.069,0.002,0.14,0.003,0.21l-0.09,0.053c-0.54-0.715-1.142-1.266-1.769-1.601     C10.156,37.752,10.589,37.098,11.127,36.779z M9.629,39.516c0.43,0.269,0.85,0.669,1.24,1.172l-1.45,0.85     C9.432,40.805,9.505,40.125,9.629,39.516z"/>
					</svg>
				</i>


				<h2 class="category__heading">
					<a href="/writings/my-latest-musings/" class="category__link">My latest musings</a>
				</h2>
				

				<p class="category__content">My thoughts on the state of the web; musings from my role as a freelance developer and notable discoveries from t'interweb.</p>


				<a href="/writings/my-latest-musings/" class="category__button  button--secondary">
					Read my latest musings
					<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
						<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
					</svg>
				</a>
			</article>





			<article class="category__item  category__item--knowledge-base">
				<i class="category__icon">
					<svg class="icon--consultancy" height="160" width="160" viewBox="-2.404 -27.524 100 100" style="margin-top: -5px; margin-left: 3px;">
						<path d="M47.589,18.511c-7.301,0-29.327,2.295-29.206,9.696c0.116,7.273,0,14.104,0,14.104h58.3c0,0,0-1.982,0-14.104  C76.684,20.834,54.863,18.511,47.589,18.511z"/>
						<path d="M95.191,21.155L47.596,0L0,21.155l6.612,2.937v12.93H5.287v2.643l-2.644,5.289h10.579l-2.646-5.289v-2.643H9.255V25.267  l6.504,2.891c-0.007-0.639-0.013-1.213-0.023-1.715c-0.129-8.073,23.895-10.577,31.86-10.577c7.933,0,31.729,2.534,31.729,10.577  c0,0.572,0,1.162,0,1.764L95.191,21.155z"/>
					</svg>
				</i>


				<h2 class="category__heading">
					<a href="/writings/knowledge-base/" class="category__link">Knowledge base</a>
				</h2>
				

				<p class="category__content">The Knowledge Base is a bank of useful snippets and tutorials for front end developers. It's still very much in its infancy though!</p>


				<a href="/writings/knowledge-base/" class="category__button  button--secondary">
					View the knowledge base
					<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
						<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
					</svg>
				</a>
			</article>





			<article class="category__item  category__item--mangui">
				<i class="category__icon">
					<svg class="icon--css" width="120" height="133" viewBox="5.0 -4.0081 100.0 119.81385" style="margin-top: 10px; margin-left: 30px;">
						<path d="M33.923,93.617H23.51c-7.043,0-12.798-4.2-12.798-14.593V61.835c0-6.204-8.241-6.898-10.712-6.898V43.54   c2.471,0,10.712-0.695,10.712-6.893V19.358c0-10.391,5.462-14.491,12.798-14.491h10.413v11.392h-4.858   c-6.048,0-6.458,3.695-6.458,6.394v16.095c-0.085,9.298-5.742,9.991-9.913,10.397v0.19c4.169,0.308,9.828,1.009,9.913,10.307   v16.186c0,2.7,0.41,6.394,6.458,6.394h4.858V93.617z"/>
						<path d="M66.064,4.867h10.712c6.744,0,12.5,4.206,12.5,14.59v17.19c0,6.197,7.94,6.893,10.724,6.893v11.398   c-2.783,0-10.724,0.693-10.724,6.899v17.288c0,10.391-5.456,14.493-12.792,14.493h-10.42V82.225h4.861   c6.055,0,6.449-3.694,6.449-6.394V59.732c0.098-9.294,5.75-9.989,9.924-10.396v-0.19c-4.175-0.308-9.826-1.008-9.924-10.304V22.654   c0-2.699-0.395-6.394-6.449-6.394h-4.861V4.867z"/>
					</svg>
				</i>


				<h2 class="category__heading">
					<a href="/writings/mangui/" class="category__link">MangUI</a>
				</h2>
				

				<p class="category__content">MangUI is the new name for the, yet unreleased, Mangopear Framework. Keep upto date with all of the latest developments here.</p>


				<a href="/writings/mangui/" class="category__button  button--secondary">
					Find out more about MangUI
					<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
						<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
					</svg>
				</a>
			</article>
		</section><!-- /.container -->





		<?php

			/**
			 * Get our posts
			 */
			
			if (have_posts()) :

		?>


			<section class="lister--blog">
				<div class="container">
					<h2 class="header--stylised">Read all posts</h1>
				</div>
				

				<ul class="grid  grid--center  posts-list">
					<?php while (have_posts()) : the_post(); ?> 

						<li class="grid__item  one-whole  post  post--blog">
							<div class="container  container--text">
								<time datetime="<?php echo get_the_date('c'); ?>" class="post__date">
									<span class="date__day"><?php echo get_the_date('j'); ?></span>
									<span class="date__month"><?php echo get_the_date('M'); ?></span>
								</time>


								<article class="post__article">
									<h2 class="post__title">
										<a href="<?php the_permalink(); ?>" class="post__title__link"><?php the_title(); ?></a>
									</h2>


									<div class="post__excerpt"><?php the_excerpt(); ?></div>


									<a href="<?php the_permalink(); ?>" class="post__button  button--secondary">
										Read full article
										<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
											<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
										</svg>
									</a>
								</article>
							</div><!-- /.grid__item -->
						</li>

					<?php endwhile; ?>
				</ul>
			</section><!-- /.container -->


		<?php else : ?>

			
			<section class="lister--blog  lister--no-posts  container">
				<h1 class="header--stylised">My writings</h1>

				<h2>No posts found.</h2>
				<p>Looks like we couldn't find any posts here. Sorry!</p>
			</section>


		<?php endif; ?>





		<?php

			/**
			 * Let's get paginated
			 *
			 * [1]	Load up the global query var
			 * [2]	Create a ridiculously large number as a max for the  max number of posts
			 * [3]	Show the pagination
			 * [4]	Reset wp_query so that queries below work as expected
			 */
			
			/**
			 * [1]	Load up the global query var
			 */
		
			global $wp_query;


			/**
			 * [2]	Create a ridiculously large number as a max for the  max number of posts
			 */

			$big = 999999999;


			/**
			 * [3]	Show the pagination
			 */

			echo paginate_links( 
				array(
					'base'		=> str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
					'current'	=> max(1, get_query_var('paged')),
					'total'		=> $wp_query->max_num_pages,
					'format'	=> '?page=%#%',
					'type'		=> 'list',
					'end_size'	=> 3
				)
			);


			/**
			 * [4]	Reset wp_query so that queries below work as expected
			 */

			wp_reset_query();
		
		?>
	</main>





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>