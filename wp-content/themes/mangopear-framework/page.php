<?php
	
	/**
	 * Get the header code
	 */
	get_header();

?>





	<div class="container">
		<h1 class="title  title--page"><?php the_title(); ?></h1>


		<div class="container  container--text">
			<?php
		
				/**
				 * Default WordPress Loop call
				 */
				
				if (have_posts()) : 
					while (have_posts()) : the_post(); 

						the_content();

					endwhile;

				else :
					echo '<h2>Oh shucks, it looks like there isn\'t any content to be found here!</h2>';

				endif;

			?>
		</div>
	</div><!-- /.container -->





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>