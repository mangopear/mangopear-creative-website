jQuery.noConflict();
jQuery(document).ready(function($){

	/**
	 * Contents
	 *
	 * [1]	Open the search form
	 * [2]	Open the main nav      
	 */
	




	/**
	 * [1]	Open the search form
	 
	
	$('.js--toggle[data--js--toggle="header__search"]').click(function(){
		$('.header__nav').removeClass('js--open');
		$('.header__search, .header--main').toggleClass('js--open');
		$('.search__input').focus();
	});
	
	$('.js--close-search').click(function(){
		$('.header__nav').addClass('js--open');
		$('.header__search, .header--main').toggleClass('js--open');
	});
	*/




	/**
	 * [2]	Open the main nav
	 *
	 * 		[a]	Close the nav on page load
	
	
	$('.main-nav').addClass('main-nav--closed'); // [a]
	

	$('.js--toggle[data--js--toggle="header__navigation"]').click(function(e){
		e.stopPropagation();
		if ($('.main-nav').hasClass('main-nav--closed')) { $('.main-nav').removeClass('main-nav--closed').addClass('main-nav--open');
		} else { $('.main-nav').addClass('main-nav--closed').removeClass('main-nav--open'); }
	});


	$('html, body').click(function(e){
		e.stopPropagation();
		$('.main-nav--open').removeClass('main-nav--open');
	});
 */




	/**
	 * [2]	Open main navigation
	 */
	
	$('.js-toggle-perspective').click(function(){
		if ($(this).hasClass('open')) {
			$(this).removeClass('open');
		} else {
			$(this).addClass('open');
		}
	});

});