<section class="availability">
	<p class="availability__content">I'm currently looking for smaller projects in Q1 and Q2 and <strong>big projects in Q3 and Q4</strong>.</p>
	

	<ol class="availability__list">
		<li class="availability__item  availability__item--full">
			<strong>Jan<span class="hidden--visually">uary 2015</span></strong>
			<span class="availability__overlay">Fully available</span>
		</li>


		<li class="availability__item  availability__item--none">
			<strong>Feb<span class="hidden--visually">ruary 2015</span></strong>
			<span class="availability__overlay">Sorry, I'm fully booked!</span>
		</li>


		<li class="availability__item  availability__item--limited">
			<strong>Mar<span class="hidden--visually">ch 2015</span></strong>
			<span class="availability__overlay">Limited availability</span>
		</li>


		<li class="availability__item  availability__item--limited">
			<strong>Apr<span class="hidden--visually">il 2015</span></strong>
			<span class="availability__overlay">Limited availability</span>
		</li>


		<li class="availability__item  availability__item--limited">
			<strong>May<span class="hidden--visually"> 2015</span></strong>
			<span class="availability__overlay">Limited availability</span>
		</li>


		<li class="availability__item  availability__item--limited">
			<strong>Jun<span class="hidden--visually">e 2015</span></strong>
			<span class="availability__overlay">Limited availability</span>
		</li>


		<li class="availability__item  availability__item--full">
			<strong>Jul<span class="hidden--visually">y 2015</span></strong>
			<span class="availability__overlay">Fully available</span>
		</li>


		<li class="availability__item  availability__item--full">
			<strong>Aug<span class="hidden--visually">ust 2015</span></strong>
			<span class="availability__overlay">Fully available</span>
		</li>


		<li class="availability__item  availability__item--full">
			<strong>Sep<span class="hidden--visually">tember 2015</span></strong>
			<span class="availability__overlay">Fully available</span>
		</li>


		<li class="availability__item  availability__item--full">
			<strong>Oct<span class="hidden--visually">ober 2015</span></strong>
			<span class="availability__overlay">Fully available</span>
		</li>


		<li class="availability__item  availability__item--full">
			<strong>Nov<span class="hidden--visually">ember 2015</span></strong>
			<span class="availability__overlay">Fully available</span>
		</li>


		<li class="availability__item  availability__item--full">
			<strong>Dec<span class="hidden--visually">ember 2015</span></strong>
			<span class="availability__overlay">Fully available</span>
		</li>
	</ol>
</section>