<?php

	/**
	 * Function to decide whether or not to show the_title() in the content area
	 *
	 * @since 0.2.0-alpha
	 */
	
	function mangopear__content__title()
	{
		$get_mangopear__header_style = get_field('main-header__background-image');

		if (!$get_mangopear__header_style) :
			echo get_the_title();


		endif;
	}

?>





<header class="header--content">
	<h1>
		<?php mangopear__content__title(); ?>
	</h1>
</header>


<article class="article--main">
	<?php
		/**
		 * Standard WordPress Post loop
		 */
		
		if (have_posts()) :
			while (have_posts()) :

				the_post();
				the_content();

			endwhile;
		endif;
	?>
</article>