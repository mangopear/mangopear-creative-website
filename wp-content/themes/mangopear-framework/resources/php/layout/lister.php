<?php

	/**
	 * Function to decide whether or not to show the_title() in the content area
	 *
	 * @since 0.2.0-alpha
	 */
	
	function mangopear__content__title()
	{
		$get_mangopear__header_style = get_field('main-header__background-image');

		if (!$get_mangopear__header_style) :
			echo get_the_title();


		endif;
	}

?>





<header class="header--content">
	<h1>
		<?php mangopear__content__title(); ?>
	</h1>
</header>


<?php

	if (have_posts()) :
		while (have_posts()) :

			the_post();

?>


			<article class="post">
				<section class="post__article">
					<a href="<?php the_permalink(); ?>">
						<header>
							<h2>
								<?php the_title(); ?>
							</h2>
						</header>


						<?php if (get_field('cpt__post-type')) : ?>
							<p class="post__type">
								<?php the_field('cpt__post-type'); ?>
							</p>
						<?php endif; ?>


						<section class="article__excerpt">
							<?php the_excerpt(); ?>
						</section><!-- /.article__excerpt -->
					</a>
				</section><!-- /.post__article -->


				<footer class="article-meta">
					<time datetime="<?php echo get_the_date('Y-n-j'); ?>">
						<span class="time__day">
							<?php echo get_the_date('j') ?>
						</span>

						<span class="time__month">
							<?php echo get_the_date('M'); ?>
						</span>
					</time>


					<p class="article-meta__link  article-meta__link--comments">
						<a href="<?php the_permalink(); ?>#jump-to--comments">
							<strong>
								<?php comments_number( '0', '1', '%' ); ?> 
							</strong>
							comments
						</a>
					</p>


					<p class="article-meta__link  article-meta__link--author">
						Posted by <strong><?php the_author(); ?></strong>
					</p>
				</footer>
			</article>

<?php

		endwhile;
	endif;

?>