<h2 class="hidden--visually">
	Sidebar
</h2>


<section class="side__pod  side__pod--about">
	<h3>About me</h3>

	<p>
		I'm a multi-discipline digital <br/>
		designer &amp; developer from the <br/>
		United Kingdom. You'll often find <br/>
		me designing for print too. I <br/>
		<a href="/knowledge-base/">share code</a>, <a href="/blog/">write</a> and <a href="http://twitter.com/MangopearUK">tweet cool stuff</a>. You <br/>
		can even <a href="/hire-me/">hire me</a> for your next project.
	</p>
</section>