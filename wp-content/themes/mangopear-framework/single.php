<?php
	
	/**
	 * Get the header code
	 */
	get_header();

?>





	<div class="container">
		<h1 class="header--stylised">
			<?php the_title(); ?>
		</h1>


		<div class="typography--standard">
			<?php
				
				/**
				 * Default WordPress Loop call
				 */
				
				if (have_posts()) : 
					while (have_posts()) : the_post(); 

						the_content();

					endwhile;

				else :
					echo '<h2>Oh shucks, it looks like there isn\'t any content to be found here!</h2>';

				endif;

			?>
		</div>
	</div><!-- /.container -->





	<section class="comments-row">
		<?php comments_template(); ?>
	</section>





	<section class="callout  callout--comments">
		<div class="container">
		<?php

				/**
				 * Comments form
				 */
				
				if ($post->comment_status == 'open') :

			?>


					<h3>Leave a reply</h3>


					<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" class="comments__form">
						<fieldset>
							<div class="frm_form_field">
								<label for="author" class="frm_primary_label" style="display: none;">Name</label>
								<input type="text" id="author" name="author" value="<?php echo $comment_author; ?>" placeholder="Name *">
							</div>


							<div class="frm_form_field">
								<label for="email" class="frm_primary_label" style="display: none;">Email</label>
								<input type="email" id="email" name="email" value="<?php echo $comment_author_email; ?>" placeholder="Email *">
							</div>


							<div class="frm_form_field">
								<label for="url" class="frm_primary_label" style="display: none;">Website</label>
								<input type="text" id="url" name="url" value="<?php echo $comment_author_url; ?>" placeholder="Website">
							</div>


							<div class="frm_form_field">
								<label for="comment" class="frm_primary_label" style="display: none;">Comment</label>
								<textarea id="comment" name="comment" placeholder="Comment *" rows="10"></textarea>
							</div>


							<div class="frm_form_field">
								<button name="submit" id="submit">Add comment</button>
								<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
							</div>
						</fieldset>

						<?php do_action('comment_form', $post->ID); ?>
					</form>

			<?php endif; ?>
		</div>
	</section>





	<section class="back-to  back-to--blog">
		<a href="/writings/my-latest-musings/" class="button">
			<i class="fa  fa-chevron-circle-left"></i>
			<span class="button__text">Back to my latest musings</span>
		</a>
	</section><!-- /.back-to -->





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>