<?php

	/**
	 * Template name: [Detail] Homepage
	 */
	
	get_header();

?>





	<main class="main">
		<section class="panel  panel--home-intro  panel--secondary">
			<div class="container">
				<div class="grid  grid--center">
					<div class="grid__item  one-half  palm--one-whole  lap--one-whole">
						<h1 class="beta  panel__heading">I'm a designer, developer and consultant.</h1>
						<p class="home-intro__content">I'm based in Winchester (UK); highly motivated; self employed and enjoy working with great companies. I feel passionately about the quality of work that I produce, always going the extra mile for my clients.</p>
						<p class="home-intro__content">When I'm not busy, you’ll find me <a href="/writing/" title="Read my articles" class="button--tertiary">writing</a>, <a href="http://github.com/mangopearuk" target="_blank" title="View my work on Github" class="button--tertiary">sharing code</a> and <a href="http://twitter.com/MangopearUK" target="_blank" title="Read my tweets" class="button--tertiary">tweeting</a>.</p>
					</div><!-- /.grid__item -->
				</div><!-- /.grid -->
			</div><!-- /.container -->
		</section><!-- /.panel -->





		<article class="panel  panel--home-services  panel--tertiary">
			<div class="container">
				<h2 class="home-services__header  panel__heading">What I do and why I love it...</h2>
				

				<div class="grid  grid--center">
					<div class="grid__item  one-third  lap--one-half  palm--one-whole">
						<h3 class="home-services__heading">
							<svg class="home-services__heading__icon  icon--consultancy" viewBox="-2.404 -27.524 100 100">
								<path d="M47.589,18.511c-7.301,0-29.327,2.295-29.206,9.696c0.116,7.273,0,14.104,0,14.104h58.3c0,0,0-1.982,0-14.104  C76.684,20.834,54.863,18.511,47.589,18.511z"/>
								<path d="M95.191,21.155L47.596,0L0,21.155l6.612,2.937v12.93H5.287v2.643l-2.644,5.289h10.579l-2.646-5.289v-2.643H9.255V25.267  l6.504,2.891c-0.007-0.639-0.013-1.213-0.023-1.715c-0.129-8.073,23.895-10.577,31.86-10.577c7.933,0,31.729,2.534,31.729,10.577  c0,0.572,0,1.162,0,1.764L95.191,21.155z"/>
							</svg>
							<span class="home-services__heading__text">Consultancy</span>
						</h3>
						<p class="home-services__content"><em>Define:</em></br>A professional practice that gives expert advice within a particular field.</p>
					</div><!-- /.grid__item -->


					<div class="grid__item  one-third  lap--one-half  palm--one-whole">
						<h3 class="home-services__heading">
							<svg class="home-services__heading__icon  icon--development" viewBox="0 0 40 40">
								<path d="M26.7,13.4L25.2,16l6.6,4l-6.6,4l1.5,2.6l8.7-5.3v-2.6  L26.7,13.4z M13.3,13.4l-8.7,5.3v2.6l8.7,5.3l1.5-2.6l-6.6-4l6.6-4L13.3,13.4z M16.3,27.6l2.9,0.7l3.5-15.8l-2.9-0.7L16.3,27.6z"/>
							</svg>
							<span class="home-services__heading__text">Development</span>
						</h3>
						<p class="home-services__content"><em>Define:</em><br/>The process of developing or being developed.</p>
					</div><!-- /.grid__item -->


					<div class="grid__item  one-third  lap--one-half  palm--one-whole">
						<h3 class="home-services__heading">
							<svg class="home-services__heading__icon  icon--design" viewBox="0 0 32 32">
								<path id="pensil" d="M25.517,3.254l-3.385-2.127c-0.854-0.536-1.979-0.278-2.517,0.575l-1.334,2.123l6.474,4.066L26.09,5.77  C26.626,4.915,26.371,3.789,25.517,3.254z M6.763,22.168l6.474,4.064L23.788,9.436L17.31,5.368L6.763,22.168z M5.772,27.334  l-0.143,3.818l3.379-1.787l3.14-1.658l-6.246-3.926L5.772,27.334z"/>
							</svg>
							<span class="home-services__heading__text">Design</span>
						</h3>
						<p class="home-services__content"><em>Define:</em></br>A professional practice that gives expert advice within a particular field.</p>
					</div><!-- /.grid__item -->
				</div><!-- /.grid -->


				<a href="/what-i-do/" class="button--secondary  button--home-services">
					<span class="button__text">Read more about what I do</span>
					<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
						<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
					</svg>
				</a>
			</div><!-- /.container -->
		</article><!-- /.panel -->





		<section class="panel  panel--testimonial  panel--secondary">
			<div class="container">
				<blockquote class="testimonial">
					<article class="testimonial__content">
						<p>Andi North has been responsible for the administrative phased rebranding of Jeakins Coach Travel. This has involved front of house and back office publications.</p>
						<p>We are extremely satisfied with the consistently high standard of his product and the fact that Andi has grown to share the Board's values, 'isms' and expectations.</p>
					</article>
					

					<footer class="testimonial__footer">
						<cite class="testimonial__from">
							<strong class="testimonial__name">Mark R Self,</strong>
							<em class="testimonial__name--company">Jeakins Coach Travel</em>
						</cite>
					</footer>
				</blockquote>
			</div><!-- /.container -->
		</section><!-- /.panel -->
	</main><!-- /.main -->





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>