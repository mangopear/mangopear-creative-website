<?php
	
	/**
	 * Template name: [Lister] Legal stuff
	 */
	get_header();

?>





	<div class="container">
		<h1 class="header--stylised">
			<?php the_title(); ?>
		</h1>
	</div><!-- /.container -->


	<ul class="grid  posts-list">
		<?php

			$legal_posts = array(212,213,214);
			foreach ($legal_posts as $post) :

		?>
		<li class="post  post--blog">
			<div class="typography--standard">
				<time datetime="<?php echo get_the_date('c', $post); ?>" class="post__date">
					<span class="date__day"><?php echo get_the_date('j', $post); ?></span>
					<span class="date__month"><?php echo get_the_date('M', $post); ?></span>
				</time>


				<article class="post__article">
					<h2 class="post__title">
						<a href="<?php echo get_permalink($post); ?>" class="post__title__link"><?php echo get_the_title($post); ?></a>
					</h2>


					<div class="post__excerpt"><?php the_field('excerpt', $post); ?></div>


					<a href="<?php echo get_permalink($post); ?>" class="post__button  button--secondary">
						Read all about my <span style="text-transform: lowercase;"><?php echo get_the_title($post); ?></span>
						<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
							<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
						</svg>
					</a>
				</article>
			</div><!-- /.typography-\-standard -->
		</li>
		<?php endforeach; ?>
	</ul>





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>