<?php
	
	/**
	 * Template name: What I do
	 * 
	 * Get the header code
	 */
	
	get_header();

?>





	<main class="main">
		<div class="container">
			<h1 class="main__title  visually-hidden"><?php the_title(); ?></h1>


			<div class="panel  panel--secondary">
				<div class="grid  grid--center">
					<div class="grid__item  two-thirds  lap--one-whole  palm--one-whole">
						<h1 class="what-i-do__heading  beta">Hi, I'm Andi.</h1>
						<p class="lede">A highly motivated designer, developer and consultant. I'm self employed and enjoy working with some great companies, ranging from independent bus companies to high-end suppliers to online startups.</p>
						<p>I provide consultancy to organisations on how they can get the most out of their website by understanding where it is currently letting them down and advising them on how to address those issues. I also design and build websites for small and medium sized organisations.</p>
						<p>I have three years of agency experience building websites for leading retailers and strategists in the UK, whilst also gaining experience of working with smaller, local companies and charities.</p>
						<p>I don't just work online though, I have experience designing for print too - including branding; timetable design and brochure production. Look at my portfolio to <a href="/portfolio/" title="View my portfolio" class="button--tertiary">see more of what I have done</a>.</p>
						<p>I feel passionately about the quality of work that I produce, always going the extra mile for my clients. It may sometimes takes me a little longer than most to create the work I do as I am picky about the little details, always ensuring things are done right.</p>
						<p>I'm available for hire and looking to work with organisations that are open to change and seeking to improve their professional appearance.</p>
						<p>I'm a freelancer because I thrive on the variety of work I do and enjoy meeting interesting new companies and people; working on quality projects with clients that value the worth of great design and realise the power it holds within their business.</p>
						<p class="lede">If that sounds like you and your project, <a href="/contact/hire/" title="Hire me" class="button--tertiary">get in touch</a> today!</p>
					</div><!-- /.grid__item -->
				</div><!-- /.grid -->
			</div><!-- /.panel -->
		</div><!-- /.container -->





		<section class="panel  panel--testimonial  panel--tertiary">
			<div class="container">
				<blockquote class="testimonial">
					<article class="testimonial__content">
						<p>Andi North has been responsible for the administrative phased rebranding of Jeakins Coach Travel. This has involved front of house and back office publications.</p>
						<p>We are extremely satisfied with the consistently high standard of his product and the fact that Andi has grown to share the Board's values, 'isms' and expectations.</p>
					</article>
					

					<footer class="testimonial__footer">
						<cite class="testimonial__from">
							<strong class="testimonial__name">Mark R Self,</strong>
							<em class="testimonial__name--company">Jeakins Coach Travel</em>
						</cite>
					</footer>
				</blockquote>
			</div><!-- /.container -->
		</section><!-- /.panel -->





		<section class="panel  panel--primary  panel--hire-me">
			<div class="container">
				<div class="grid">
					<div class="grid__item  three-quarters  palm--one-whole  hire-me__content">
						<h2 class="hire-me__heading">Intrigued by what you have read so far?</h2>
						<p class="hire-me__text">Get in touch with me to see how I can make your branding; marketing and website do more for your business.</p>
					</div><!-- /.grid__item -->


					<div class="grid__item  one-quarter  palm--one-whole  hire-me__call-to-action">
						<a href="/contact/hire/" class="button--primary  hire-me__button">
							<span class="button__text">Let's talk</span>
							<svg class="button__icon--right  icon--chevron-right" viewBox="0 0 16 16" width="20" height="20">
								<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
							</svg>
						</a><!-- /.hire-me__button -->
					</div><!-- /.grid__item -->
				</div><!-- /.grid -->
			</div><!-- /.container -->
		</section><!-- /.panel -->





		<section class="panel  panel--testimonial  panel--secondary">
			<div class="container">
				<blockquote class="testimonial">
					<article class="testimonial__content">
						<p>Andi has been a tremendous asset helping us create and publish our website as well as being a consultant on various other issues, such a social media and graphics.</p>
						<p>He has been very helpful, delivering on time and working with us to help create what we want.</p>
					</article>
					

					<footer class="testimonial__footer">
						<cite class="testimonial__from">
							<strong class="testimonial__name">Felicity Crabb,</strong>
							<em class="testimonial__name--company">The Edge Project</em>
						</cite>
					</footer>
				</blockquote>
			</div><!-- /.container -->
		</section><!-- /.panel -->
	</main>





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>