<?php
	
	/**
	 * Template name: [Detail] Case study: Silk
	 */
	

	get_header();

?>





	<main class="main">
		<header class="panel  panel--tertiary  panel--portfolio-header  panel--portfolio-header--silk">
			<div class="container">
				<h1 class="portfolio-header__title">Silk Web Toolkit</h1>
			</div><!-- /.container -->
		</header>





		<article class="portfolio-content">
			<section class="panel  panel--case-study-screens  panel--tertiary">
				<img class="case-study-screens__image" src="/wp-content/themes/mangopear-framework/resources/images/_uploaded-to-wordpress/screens--silk.png" alt="The Silk Web Toolkit" title="The Silk Web Toolkit">
			</section>





			<section class="panel  panel--case-study-section">
				<div class="container  container--text  type--centre">
					<h2>Working with Gardbus</h2>
					<p class="lead">Since 2012, I have been working with Gardbus, a local independent bus company, who operate a modern fleet of buses in and around Ringwood, The New Forest and East Dorset.</p>
					<p>My first task was to create a distinctive brand ident for Gardbus, ensuring that they stand out against the other bus and coach operators in the area. This was accompanied by a tri-tone vehicle livery that reflects the modern and professional approach of the company.</p>
					<p>Following the success of the branding exercise, we kicked off the website refresh, incorporating the new branding and colour scheme whilst enhancing the content on the website by adding interactive timetables and route maps.</p>
							

					<p>
						<a href="http://gardbus.co.uk/" target="_blank" class="button--secondary">
							<span class="button__text">View the Gardbus website</span>
							<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
								<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
							</svg>
						</a>
					</p>
				</div><!-- /.container -->
			</section><!-- /.panel -->





			<section class="grid  case-study__logos">
				<div class="grid__item  one-whole  case-study__logo  case-study__logo--gardbus  case-study__logo--light">
					<svg viewBox="0 0 395.6 161" width="400px" height="100px" class="logo  logo--silk">
						<path fill="#AFDFF9" d="M235.9,127.8c0-9.8,7.2-16.6,16.2-16.6c5.1,0,9,2.3,11.7,5.7l-2.3,1.5c-2-2.8-5.5-4.7-9.4-4.7 c-7.5,0-13.3,5.7-13.3,14.1c0,8.3,5.9,14.1,13.3,14.1c3.8,0,7.3-1.9,9.4-4.7l2.4,1.4c-2.9,3.5-6.6,5.7-11.7,5.7 C243.1,144.4,235.9,137.7,235.9,127.8z"/>
						<path fill="#AFDFF9" d="M313.3,143.8v-28.6l-11.7,28.6h-1.1l-11.7-28.6v28.6h-2.7v-32h4.1l10.9,26.6l10.8-26.6h4.1v32H313.3z"/>
						<path fill="#AFDFF9" d="M339.1,139.3l1.8-2.1c2.1,2.4,5.4,4.7,10,4.7c6.5,0,8.4-3.6,8.4-6.4c0-9.4-19.2-4.5-19.2-15.7 c0-5.2,4.7-8.5,10.5-8.5c4.8,0,8.4,1.7,10.8,4.5l-1.8,2c-2.3-2.8-5.6-4-9.2-4c-4.3,0-7.5,2.4-7.5,5.9c0,8.2,19.2,3.7,19.2,15.6 c0,4.1-2.7,9.1-11.4,9.1C345.5,144.4,341.5,142.2,339.1,139.3z"/>
						<circle fill-rule="evenodd" clip-rule="evenodd" fill="#AFDFF9" cx="80.5" cy="80.5" r="80.5"/>
						<path fill="#156998" d="M206.9,93.5l9.4-15.7c4.9,3.9,15.6,7.9,22.6,7.9c4.8,0,6.2-1.2,6.2-2.7c0-6-35.6,1.7-35.6-24.6 c0-11.3,9.9-21.2,28.1-21.2c10.5,0,19.9,3.3,27.2,8.1l-8.6,15.2c-3.9-3.3-11.2-6.5-18.5-6.5c-3.4,0-5.9,1-5.9,2.7 c0,5.2,35.6-1.3,35.6,24.8c0,12.5-11.2,21.6-29.8,21.6C226.1,103.1,213.7,99.1,206.9,93.5z"/>
						<path fill="#156998" d="M267.4,21.1c0-7.3,5.9-13.1,13.1-13.1s13.1,5.9,13.1,13.1s-5.9,13.1-13.1,13.1S267.4,28.4,267.4,21.1z M268.8,101.5V38.8h23.4v62.8H268.8z"/>
						<path fill="#156998" d="M298,101.5V14.8h23.4v86.7H298z"/>
						<path fill="#156998" d="M366,101.5l-10.8-20.3l-4.5,6.1v14.2h-23.4V14.8h23.4v46.3l14.4-22.4h28.2l-21.6,28.6l23,34.2H366z"/>

						<defs>
							<circle id="SVGID_1_" cx="80.5" cy="80.5" r="80.5"/>
						</defs>
						
						<clipPath id="SVGID_2_">
							<use xlink:href="#SVGID_1_"  overflow="visible"/>
						</clipPath>

						<g clip-path="url(#SVGID_2_)">
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#156998" d="M1.1,75.9c6.9,1.7,13.7,3.5,20.6,5.2 c34.7,8.5,54.1,31.4,61.6,65.5c3.4,15.3,3.3,15.4-13,13.9C30,157-5,114.1,1.1,75.9z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#156998" d="M86.8-0.2C127.4,2,161.1,37,162.2,77.2c0.1,4.8-1.3,5.9-6.2,4.9 c-2.1-0.4-4.3-1-6.4-1.5c-12.1-2.8-20.7-6.8-24.4-20.3c-4-14.7-16.8-22.8-30.1-28.5c-6.9-3-10.3-6.9-11.8-14 c-0.8-3.8-1.9-7.1-2.3-10.4C80.2-0.6,81.3-1.3,86.8-0.2z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#156997" d="M70.4,0.1c-5.7,15.1,7.9,29.8,1.6,42.5c-4.4-6.7-5.5-14-7.2-21.1 c-0.9-3.6-2-8.5-6.9-7.7c-4.2,0.6-4.4,5.1-4.5,8.7c-0.3,9.5,2.3,18.5,5,27.4c1.8,5.9,0.2,7.7-5.8,5.6c-7-2.5-14.3-3.9-21.7-4.7 c-2.4-0.3-5-0.5-7.3-0.2c-3.2,0.5-7.2,1.2-7.4,5.2c-0.1,3.4,3.3,4.8,6.3,5.6c6.1,1.6,12.2,3.1,18.3,4.7c1.9,0.5,5,0.5,4.1,3.5 c-0.6,1.9-2.5,3.4-5.3,2.5c-11.3-3.5-22.7-6.1-34.6-6.4c-4.5-0.1-2.3-4.6-1.5-7.1C11.7,29.1,31.2,10.4,60.7,2 C63.3,1.2,66.1,0.9,70.4,0.1z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#156997" d="M113.3,136.4c-1.8-8.3-3.6-16.6-5.4-24.9 c-0.5-2.3-2.7-4.6-0.2-6.8c2-1.7,4.2-0.3,6.3,0.3c7.1,1.9,14.2,4,21.7,4.4c0.8,0,1.6,0.1,2.5,0.2c4.3,0.5,10.2,0.8,11-4.7 c0.8-5.6-5.1-6.1-9.3-7.2c-4.5-1.2-9.1-2.1-13.6-3.3c-1.7-0.5-5-0.4-3.8-3.6c0.8-2,2.6-3,5.4-2.2c10,2.9,20,5.8,30.6,5.5 c2.6-0.1,2,1.6,1.8,3.3c-2.8,25.2-32.6,56.6-58.3,61.2c-3.5,0.6-5.8,0.5-5.5-4.1c0.5-10-2.4-19.5-5.1-29c-0.8-2.7-2.1-5.6,1.6-6.9 c4.8-1.6,3.5,3.2,4.2,5.1c1.6,4.6,2.2,9.6,3.6,14.3c1.1,3.8,3.4,9.8,8.9,8.1C114.4,144.7,114.1,141.7,113.3,136.4z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#166491" d="M98.9,50.7c-3-0.4-4.5-2-4.2-4.9c0.4-2.9,2.2-4.4,5-4.1 c2.6,0.2,3.7,2.1,3.7,4.5C103.4,49.1,101.6,50.5,98.9,50.7z"/>
							<path fill-rule="evenodd" clip-rule="evenodd" fill="#166592" d="M116.1,59.3c3,0.1,5.1,1.5,4.9,4.4c-0.2,2.6-1.9,4.4-4.9,4.3 c-2.6-0.1-4.2-1.7-4.3-4C111.7,61.2,113.5,59.5,116.1,59.3z"/>
						</g>
					</svg>
				</div>
			</section><!-- /.panel -->





			<section class="panel  panel--secondary  panel--case-study-section">
				<div class="container">
					<div class="grid  grid--center  grid--middle">
						<div class="grid__item  one-third  lap--one-whole  palm--one-whole">
							<img src="/wp-content/themes/mangopear-framework/resources/images/_uploaded-to-wordpress/portfolio--gardbus__timetables.jpg" class="section__image" alt="Gardbus timetables" title="Gardbus timetables">
						</div><!-- /.grid__item -->


						<div class="grid__item  one-half  lap--one-whole  palm--one-whole">
							<h2>In addition to the website...</h2>
							<p class="lead">To maintain their professional appearance, Gardbus seeked my services to design a series of timetables for print.</p>
							<p>The design of these timetables has matured over time, leading to a beautiful style bespoke to Gardbus. Following feedback from passengers, a leaflet is produced three times a year which contains all Gardbus&nbsp;timetables.</p>
							

							<p>
								<a href="/wp-content/uploads/2015/02/Gardbus-timetable-routes-125-and-126-valid-from-5th-January-2015.pdf" target="_blank" class="button--secondary">
									<span class="button__text">View timetables</span>
									<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
										<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
									</svg>
								</a>
							</p>
						</div><!-- /.grid__item -->
					</div><!-- /.grid -->
				</div><!-- /.container -->
			</section><!-- /.panel -->





			<section class="panel  panel--tertiary  panel--case-study-section">
				<div class="container">
					<div class="grid  grid--center  grid--middle">
						<div class="grid__item  one-half  lap--one-whole  palm--one-whole">
							<h2>The website was reviewed in 2014.</h2>
							<p class="lead">Following the success of the initial website, we investigated what could be done to improve it.</p>
							<p>The review pulled up some issue on tablet devices, which were either shown a blown up mobile view or shrunken down desktop view - neither being&nbsp;optimal.</p>
							<p>When reviewing Google Analytics, we noticed some issues with certain templates not performing as required and that the average number of pages/visit was incredibly low. <em>Something that needed to be improved.</em></p>
						</div><!-- /.grid__item -->


						<div class="grid__item  one-third  lap--one-whole  palm--one-whole  align--right">
							<img src="/wp-content/themes/mangopear-framework/resources/images/_uploaded-to-wordpress/portfolio--gardbus__features.jpg" class="section__image" alt="Gardbus timetables" title="Gardbus timetables">
						</div><!-- /.grid__item -->
					</div><!-- /.grid -->
				</div><!-- /.container -->
			</section><!-- /.panel -->





			<section class="panel  panel--secondary  panel--case-study-section">
				<div class="container">
					<div class="grid  grid--center  grid--middle">
						<div class="grid__item  one-third  lap--one-whole  palm--one-whole">
							<img src="/wp-content/themes/mangopear-framework/resources/images/_uploaded-to-wordpress/portfolio--gardbus__post-review.jpg" class="section__image" alt="Change was needed!" title="Change was needed!">
						</div><!-- /.grid__item -->


						<div class="grid__item  one-half  lap--one-whole  palm--one-whole">
							<h2>Change was needed...</h2>
							<p class="lead">The review highlighted quite a few areas that needed change. Big change.</p>
							<p>A decision was taken to redesign the website, to match the look and feel of the website with the latest timetable&nbsp;leaflets.</p>
							<p>The redesign would also allow for the content to be updated, as was suggested following the review. New pages need to be added, for instance for <a href="http://gardbus.co.uk/taxi/" target="_blank" class="button--tertiary">Gardtaxi</a> and <a href="http://gardbus.co.uk/fares/gardrider/" target="_blank" class="button--tertiary">GardRider</a> (Gardbus season tickets), existing pages were to be edited, whilst redundant pages were to be removed.</p>
						</div><!-- /.grid__item -->
					</div><!-- /.grid -->
				</div><!-- /.container -->
			</section><!-- /.panel -->





			<section class="panel  panel--tertiary  panel--case-study-section">
				<div class="container">
					<div class="grid  grid--center  grid--middle">
						<div class="grid__item  one-half  lap--one-whole  palm--one-whole">
							<h2>How it happened</h2>
							<p class="lead"></p>
							<p></p>
							<p></p>
						</div><!-- /.grid__item -->


						<div class="grid__item  one-third  lap--one-whole  palm--one-whole">
							<img src="/wp-content/themes/mangopear-framework/resources/images/_uploaded-to-wordpress/portfolio--gardbus__progression.jpg" class="section__image" alt="See how the design progressed" title="See how the design progressed">
						</div><!-- /.grid__item -->
					</div><!-- /.grid -->
				</div><!-- /.container -->
			</section><!-- /.panel -->





			<section class="panel  panel--secondary  panel--case-study-section">
				<div class="container">
					<div class="grid  grid--center  grid--middle">
						<div class="grid__item  one-third  lap--one-whole  palm--one-whole">
							<img src="/wp-content/themes/mangopear-framework/resources/images/_uploaded-to-wordpress/portfolio--gardbus__results.png" class="section__image" alt="Look at how the Gardbus statistics increased!" title="Look at how the Gardbus statistics increased!">
						</div><!-- /.grid__item -->


						<div class="grid__item  one-half  lap--one-whole  palm--one-whole">
							<h2>The results...</h2>
							<p class="lead"></p>
							<p></p>
							<p></p>
						</div><!-- /.grid__item -->
					</div><!-- /.grid -->
				</div><!-- /.container -->
			</section><!-- /.panel -->
		</article><!-- /.portfolio-content -->





		<section class="panel  panel--testimonial  panel--tertiary">
			<div class="container">
				<blockquote class="testimonial">
					<article class="testimonial__content">
						<p>We've been working with Andi since 2012 and we're extremely impressed with all of the work he has produced for us.</p>
						<p>His insight and creativity has vastly improved our website and we're seeing impressions increase month on month.</p>
					</article>
					

					<footer class="testimonial__footer">
						<cite class="testimonial__from">
							<strong class="testimonial__name">Simon Gard,</strong>
							<em class="testimonial__name--company">Gardbus</em>
						</cite>
					</footer>
				</blockquote>
			</div><!-- /.container -->
		</section>
	</main>





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>