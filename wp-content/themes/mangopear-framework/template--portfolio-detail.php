<?php
	
	/**
	 * Template name: [Detail] Portfolio & Case Studies
	 * 
	 * Get the header code
	 */
	get_header();

?>





	<header class="case-study__title  portfolio__item--<?php the_field('portfolio__class'); ?>">
		<div class="container">
			<h1 class="case-study__title-text">
				<span class="case-study__title__label">Case study&nbsp;</span>
				<span class="case-study__title__content"><?php the_title(); ?></span>
			</h1>


			<?php if (get_field('portfolio__url')) : ?>
				<a href="<?php the_field('portfolio__url'); ?>" class="button  button--rounded  case-study__title__button" target="_blank">
					<span class="button__text">View the website</span>
					<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="16">
						<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
					</svg>
				</a>
			<?php endif; ?>
		</div>
	</header>





	<div class="typography--standard">
		<?php
	
			/**
			 * Default WordPress Loop call
			 */
			
			if (have_posts()) : 
				while (have_posts()) : the_post(); 

					the_content();

				endwhile;

			else :
				echo '<h2>Oh shucks, it looks like there isn\'t any content to be found here!</h2>';

			endif;

		?>
	</div><!-- /.typography-\-standard -->





	<div class="container">
		<article class="portfolio__item--featured">
			<a href="/portfolio/gardbus/" class="portfolio__link--featured">
				<h2 class="portfolio__title--featured"><span class="title__case-study-next">Next case study:&nbsp;</span>Gardbus</h2>
				<p class="portfolio__content--featured">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. <strong>Read more about this successful project.</strong></p>
				<span class="portfolio__button--featured">
					<i class="portfolio__button__icon">
						<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="23" height="23">
							<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
						</svg>
					</i>
					Read case study
				</span>
			</a>
		</article>
	</div>





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>