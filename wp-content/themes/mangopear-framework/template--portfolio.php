<?php
	
	/**
	 * Template name: Portfolio & Case Studies
	 * 
	 * Get the header code
	 */
	get_header();

?>





	<main class="main">
		<div class="container">
			<h1 class="title  title--page">Portfolio &amp; case studies</h1>





			<div class="grid">
				<div class="grid__item  one-third  lap--one-half  palm--one-whole">
					<article class="clearfix  portfolio-item  portfolio-item--jeakins  portfolio-item--has-link">
						<div class="portfolio-item__header">
							<h3 class="portfolio-item__heading"><a href="/portfolio/jeakins-coach-travel/" class="portfolio-item__header__link">Jeakins Coach Travel</a></h3>
							<p class="portfolio-item__content">A family-run coach operator based in the heart of Surrey.</p>
						</div>
						
						<a href="/portfolio/jeakins-coach-travel/" class="button--secondary  portfolio-item__button">
							<span class="button__text">Read more</span>
							<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
								<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
							</svg>
						</a>
					</article>
				</div><!-- /.grid__item -->


				<div class="grid__item  one-third  lap--one-half  palm--one-whole">
					<article class="clearfix  portfolio-item  portfolio-item--gardbus  portfolio-item--has-link">
						<div class="portfolio-item__header">
							<h3 class="portfolio-item__heading"><a href="/portfolio/gardbus/" class="portfolio-item__header__link">Gardbus</a></h3>
							<p class="portfolio-item__content">A friendly, local operator running bus services in and around Ringwood.</p>
						</div>
						
						<a href="/portfolio/gardbus/" class="button--secondary  portfolio-item__button">
							<span class="button__text">Read more</span>
							<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
								<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
							</svg>
						</a>
					</article>
				</div><!-- /.grid__item -->


				<div class="grid__item  one-third  lap--one-half  palm--one-whole">
					<article class="clearfix  portfolio-item  portfolio-item--silk  portfolio-item--has-link">
						<div class="portfolio-item__header">
							<h3 class="portfolio-item__heading"><a href="/portfolio/silk-web-toolkit/" class="portfolio-item__header__link">Silk Web Toolkit</a></h3>
							<p class="portfolio-item__content">An open-source, pure HTML5 CMS built for security and performance.</p>
						</div>
						
						<a href="/portfolio/silk-web-toolkit/" class="button--secondary  portfolio-item__button">
							<span class="button__text">Read more</span>
							<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
								<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
							</svg>
						</a>
					</article>
				</div><!-- /.grid__item -->
			</div><!-- /.grid -->
		</div><!-- /.container -->
	</main><!-- /.main -->





	<footer class="panel  panel--secondary  panel--brands">
		<div class="container">
			<h2 class="panel__heading  panel__heading--portfolio">Other clients I have worked with...</h2>
			

			<div class="grid  grid--centre">
				<div class="grid__item  one-third  lap--one-half  palm--one-whole">
					<article class="clearfix  portfolio-item  portfolio-item--journee  portfolio-item--has-list">
						<div class="portfolio-item__header">
							<h3 class="portfolio-item__heading">Journee</h3>
							<p class="portfolio-item__content">A top secret product that will be launching very soon!</p>
						</div>
						
						<ul class="portfolio-item__list">
							<li>Branding</li>
							<li>Digital design</li>
							<li>Website development</li>
							<li>Systems integration</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->


				<div class="grid__item  one-third  lap--one-half  palm--one-whole">
					<article class="clearfix  portfolio-item  portfolio-item--devpod  portfolio-item--has-list">
						<div class="portfolio-item__header">
							<h3 class="portfolio-item__heading">DevPod</h3>
							<p class="portfolio-item__content">The sexiest pod of developers in the world!</p>
						</div>
						
						<ul class="portfolio-item__list">
							<li>Member</li>
							<li>Branding</li>
							<li>Website development</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->


				<div class="grid__item  one-third  lap--one-half  palm--one-whole">
					<article class="clearfix  portfolio-item  portfolio-item--theuvg  portfolio-item--has-list">
						<div class="portfolio-item__header">
							<h3 class="portfolio-item__heading">The UVG</h3>
							<p class="portfolio-item__content">Bringing the joy of music to South Hampshire.</p>
						</div>
						
						<ul class="portfolio-item__list">
							<li>Website development</li>
							<li>Digital consultancy</li>
							<li>Digital design</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->


				<div class="grid__item  one-third  lap--one-half  palm--one-whole">
					<article class="clearfix  portfolio-item  portfolio-item--edge  portfolio-item--has-list">
						<div class="portfolio-item__header">
							<h3 class="portfolio-item__heading">The EDGE Project</h3>
							<p class="portfolio-item__content">Events run by young people for young people.</p>
						</div>
						
						<ul class="portfolio-item__list">
							<li>Member</li>
							<li>Website development</li>
							<li>Digital consulting</li>
							<li>Training</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->


				<div class="grid__item  one-third  lap--one-half  palm--one-whole">
					<article class="clearfix  portfolio-item  portfolio-item--winnallca  portfolio-item--has-list">
						<div class="portfolio-item__header">
							<h3 class="portfolio-item__heading">Winnall CA</h3>
							<p class="portfolio-item__content">At the heart of the community in Winnall, Winchester.</p>
						</div>
						
						<ul class="portfolio-item__list">
							<li>Branding</li>
							<li>Digital design</li>
							<li>Website consultancy</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->


				<div class="grid__item  one-third  lap--one-half  palm--one-whole">
					<article class="clearfix  portfolio-item  portfolio-item--youthview  portfolio-item--has-list">
						<div class="portfolio-item__header">
							<h3 class="portfolio-item__heading">Youthview</h3>
							<p class="portfolio-item__content">Your view to youth services in Winnall and Highcliffe.</p>
						</div>
						
						<ul class="portfolio-item__list">
							<li>Branding</li>
							<li>Website development</li>
							<li>Digital consulting</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->
			</div><!-- /.grid -->
		</div><!-- /.container -->
	</footer><!-- /.panel -->





<?php get_footer(); ?>