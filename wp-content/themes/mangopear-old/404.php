<?php
	
	/**
	 * Get the header code
	 */
	get_header();

?>






	<header class="o-panel  o-panel--title">
		<div class="o-container  o-container--optimise-readability">
			<h1 class="c-page-title">Error 404</h1>
		</div>
	</header>





	<main class="o-panel">
		<div class="o-container  o-container--optimise-readability">
			<h2>Looks like there's an issue...</h2>
			<p>I'd suggest checking the URL of the page you're trying to access, make sure the URL is spelt correctly.</p>
			<p>It would be great if you could let me know about the issue on <a href="//twitter.com/MangopearUK" target="_blank" title="Tweet me about your issue">Twitter</a> or by using my <a href="/contact/">contact form</a></p>
		</div><!-- /.o-container -->
	</main><!-- /.o-panel -->





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>