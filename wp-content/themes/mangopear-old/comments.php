<?php

	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) :
		die('You can\'t access this file directly. You naughty little thing, you!');

		if (!empty($post->$post_password)) :
			if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) :
				echo '<p>This post is password protected. Enter the password to view comments.</p>';
				return;
			endif;

		endif;

	endif;

?>





		<h2 class="header--stylised  header--top-0">
			Comments
		</h2>





<?php

	/**
	 * Comments
	 *
	 * [1]	If comments are open, show them
	 * [2]	If there aren't any, show a message
	 * [3]	If comments are closed, show a message
	 */
	
	if ($comments) :

		$i = 1;

?>

		<ol class="list--comments">
			<?php foreach ($comments as $comment) : ?>
				<li class="comment  comment--<?php comment_id(); ?>" id="blog-comment--<?php comment_id(); ?>">
					<header class="comment__header">
						<cite>
							<?php comment_author_link(); ?>
						</cite>

						<span class="comment__integer">
							<?php echo $i; $i++; ?>
						</span>
					</header>


					<article class="comment__content">
						<?php

							/**
							 * Show comment or message
							 */
							
							if ($comment->comment_approved == '0') :
								echo 'Your comment is awaiting moderation';

							else :
								comment_text();

							endif;

						?>
					</article>


					<footer class="comment__footer">
						<a href="#comment-<?php comment_ID() ?>">
							<?php comment_date('F jS, Y') ?> at <?php comment_time() ?>
						</a>
					</footer>
				</li>
			<?php endforeach; ?>
		</ol>


<?php

	else :
		if ($post->comment_status == 'open') :
			echo '<p><strong>There are no comments.</strong></p>';

		else :
			echo '<p><strong>Comments are currently closed.</strong></p>';

		endif;

	endif;

?>