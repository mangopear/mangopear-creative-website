	<footer class="o-panel  o-panel--hire-me">
		<div class="o-container">
			<div class="o-grid  o-grid--middle">
				<div class="o-grid__item  u-three-quarters  u-portable--one-whole  c-hire-me__content">
					<h2 class="c-hire-me__heading">So you're interested in working with me?</h2>
					<p class="c-hire-me__text">Simply get in touch with me and we can arrange to meet up or have a Skype chat to discuss your requirements.</p>
				</div><!-- /.grid__item -->


				<div class="o-grid__item  u-one-quarter  u-portable--one-whole  c-hire-me__call-to-action">
					<a href="/contact/hire/" class="o-button  c-hire-me__button">
						<span class="o-button__text">Get in touch</span>
						<svg class="o-button__icon--right  o-icon--chevron-right" viewBox="0 0 16 16" width="20" height="20">
							<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
						</svg>
					</a><!-- /.hire-me__button -->
				</div><!-- /.o-grid__item -->
			</div><!-- /.o-grid -->
		</div><!-- /.o-container -->
	</footer><!-- /.o-panel -->





		<footer class="o-panel  o-panel--primary  c-panel--portfolio">
			<div class="o-container">
				<h2 class="o-panel__heading  c-panel__heading--portfolio">Portfolio &amp; case studies</h2>
				

				<div class="o-grid">
					<div class="o-grid__item  u-one-whole  u-lap--one-half  u-desk--one-third  lap--one-half  palm--one-whole">
						<article class="u-clearfix  c-portfolio-item  c-portfolio-item--jeakins  c-portfolio-item--has-link">
							<div class="c-portfolio-item__header">
								<h3 class="c-portfolio-item__heading"><a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="c-portfolio-item__header__link">Jeakins Coach Travel</a></h3>
								<p class="c-portfolio-item__content">A family-run coach operator based in the heart of Surrey.</p>
							</div>
							
							<a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="o-button  c-button--secondary  c-portfolio-item__button">
								<span class="button__text">Read more</span>
								<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
									<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
								</svg>
							</a>
						</article>
					</div><!-- /.grid__item -->


					<div class="o-grid__item  u-one-whole  u-lap--one-half  u-desk--one-third  lap--one-half  palm--one-whole">
						<article class="u-clearfix  c-portfolio-item  c-portfolio-item--gardbus  c-portfolio-item--has-link">
							<div class="c-portfolio-item__header">
								<h3 class="c-portfolio-item__heading"><a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="c-portfolio-item__header__link">Gardbus</a></h3>
								<p class="c-portfolio-item__content">A friendly, local operator running bus services in and around Ringwood.</p>
							</div>
							
							<a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="o-button  c-button--secondary  c-portfolio-item__button">
								<span class="button__text">Read more</span>
								<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
									<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
								</svg>
							</a>
						</article>
					</div><!-- /.grid__item -->


					<div class="o-grid__item  u-one-whole  u-lap--one-half  u-desk--one-third  lap--one-half  palm--one-whole">
						<article class="u-clearfix  c-portfolio-item  c-portfolio-item--silk  c-portfolio-item--has-link">
							<div class="c-portfolio-item__header">
								<h3 class="c-portfolio-item__heading"><a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="c-portfolio-item__header__link">Silk Web Toolkit</a></h3>
								<p class="c-portfolio-item__content">An open-source, pure HTML5 CMS built for security and performance.</p>
							</div>
							
							<a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="o-button  c-button--secondary  c-portfolio-item__button">
								<span class="button__text">Read more</span>
								<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
									<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
								</svg>
							</a>
						</article>
					</div><!-- /.grid__item -->
				</div><!-- /.grid -->


				<div class="o-panel__button-wrapper">
					<a href="/portfolio/" class="o-button  o-button--secondary  o-panel__button">
						<span class="o-button__text">Read my case studies</span>
						<svg class="o-button__icon--right  o-icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
							<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
						</svg>
					</a>
				</div>
			</div><!-- /.container -->
		</footer><!-- /.panel -->





		<footer class="o-panel  o-panel--contact">
			<div class="o-container">
				<div class="o-grid">
					<div class="o-grid__item  u-one-half  u-portable--one-whole">
						<h2 class="c-contact__heading  c-panel__heading--contact">Get in touch with me:</h2>


						<nav class="o-nav  o-nav--row  o-nav--social">
							<ul class="o-nav__list">
								<li class="o-nav__item  o-nav__item--twitter">
									<a title="Follow me on Twitter" href="https://twitter.com/MangopearUK" target="_blank" class="o-nav__link">
										<svg viewBox="0 0 512 512" height="60" width="60" class="o-social-icon  o-social-icon--twitter">
											<path fill="#FFF" d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z"/>
										</svg>
									</a>
								</li>


								<li class="o-nav__item  o-nav__item--facebook">
									<a title="Like me on Facebook" href="https://www.facebook.com/MangopearUK" target="_blank" class="o-nav__link">
										<svg viewBox="0 0 512 512" height="60" width="60" class="o-social-icon  o-social-icon--facebook">
											<path fill="#FFF" d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"/>
										</svg>
									</a>
								</li>


								<li class="o-nav__item  o-nav__item--google">
									<a title="Follow me on Google+" href="https://google.com/+AndiNorth" target="_blank" class="o-nav__link">
										<svg viewBox="0 0 512 512" height="60" width="60" class="o-social-icon  o-social-icon--google">
											<path fill="#FFF" d="M416.6 179.9h-41.5v41.5h-20.8v-41.5h-41.5v-20.8h41.5v-41.5h20.8v41.5h41.5V179.9zM299.2 351.6c0 31.1-28.4 69-99.9 69 -52.3 0-95.9-22.5-95.9-60.5 0-29.3 18.5-67.3 105.1-67.3 -12.9-10.5-16-25.1-8.2-41 -50.7 0-76.7-29.8-76.7-67.7 0-37 27.5-70.7 83.7-70.7 14.2 0 90 0 90 0l-20.1 21.1h-23.6c16.7 9.5 25.5 29.2 25.5 50.9 0 19.9-11 36-26.6 48.1 -27.7 21.4-20.6 33.4 8.4 54.6C289.7 309.6 299.2 326.2 299.2 351.6zM243.1 186.8c-4.2-31.8-24.9-58-49.2-58.7 -24.3-0.7-40.5 23.7-36.3 55.5 4.2 31.8 27.2 54.1 51.5 54.8C233.3 239.1 247.3 218.6 243.1 186.8zM268.1 354.8c0-26.2-23.9-51.2-64-51.2 -36.1-0.4-66.8 22.8-66.8 49.8 0 27.5 26.1 50.4 62.2 50.4C245.8 403.7 268.1 382.2 268.1 354.8z"/>
										</svg>
									</a>
								</li>


								<li class="o-nav__item  o-nav__item--instagram">
									<a href="https://instagram.com/MangopearUK/" title="Follow me on Instagram" target="_blank" class="o-nav__link">
										<svg viewBox="0 0 512 512" height="60" width="60" class="o-social-icon  o-social-icon--instagram">
											<path fill="#FFF" d="M365.3 234.1h-24.7c1.8 7 2.9 14.3 2.9 21.9 0 48.3-39.2 87.5-87.5 87.5 -48.3 0-87.5-39.2-87.5-87.5 0-7.6 1.1-14.9 2.9-21.9h-24.7V354.4c0 6 4.9 10.9 10.9 10.9H354.4c6 0 10.9-4.9 10.9-10.9V234.1H365.3zM365.3 157.6c0-6-4.9-10.9-10.9-10.9h-32.8c-6 0-10.9 4.9-10.9 10.9v32.8c0 6 4.9 10.9 10.9 10.9h32.8c6 0 10.9-4.9 10.9-10.9V157.6zM256 201.3c-30.2 0-54.7 24.5-54.7 54.7 0 30.2 24.5 54.7 54.7 54.7 30.2 0 54.7-24.5 54.7-54.7C310.7 225.8 286.2 201.3 256 201.3M365.3 398.1H146.7c-18.1 0-32.8-14.7-32.8-32.8V146.7c0-18.1 14.7-32.8 32.8-32.8h218.7c18.1 0 32.8 14.7 32.8 32.8v218.7C398.1 383.4 383.5 398.1 365.3 398.1"/>
										</svg>
									</a>
								</li>


								<li class="o-nav__item  o-nav__item--linkedin">
									<a title="Connect with me on LinkedIn" href="https://uk.linkedin.com/in/andinorth" target="_blank" class="o-nav__link">
										<svg viewBox="0 0 512 512" height="60" width="60" class="o-social-icon  o-social-icon--linkedin">
											<path fill="#FFF" d="M186.4 142.4c0 19-15.3 34.5-34.2 34.5 -18.9 0-34.2-15.4-34.2-34.5 0-19 15.3-34.5 34.2-34.5C171.1 107.9 186.4 123.4 186.4 142.4zM181.4 201.3h-57.8V388.1h57.8V201.3zM273.8 201.3h-55.4V388.1h55.4c0 0 0-69.3 0-98 0-26.3 12.1-41.9 35.2-41.9 21.3 0 31.5 15 31.5 41.9 0 26.9 0 98 0 98h57.5c0 0 0-68.2 0-118.3 0-50-28.3-74.2-68-74.2 -39.6 0-56.3 30.9-56.3 30.9v-25.2H273.8z"/>
										</svg>
									</a>
								</li>


								<li class="o-nav__item  o-nav__item--github">
									<a title="View my excellent work on Github" href="https://github.com/mangopearuk/" target="_blank" class="o-nav__link">
										<svg viewBox="0 0 512 512" height="60" width="60" class="o-social-icon  o-social-icon--github">
											<path fill="#FFF" d="M256 70.7c-102.6 0-185.9 83.2-185.9 185.9 0 82.1 53.3 151.8 127.1 176.4 9.3 1.7 12.3-4 12.3-8.9V389.4c-51.7 11.3-62.5-21.9-62.5-21.9 -8.4-21.5-20.6-27.2-20.6-27.2 -16.9-11.5 1.3-11.3 1.3-11.3 18.7 1.3 28.5 19.2 28.5 19.2 16.6 28.4 43.5 20.2 54.1 15.4 1.7-12 6.5-20.2 11.8-24.9 -41.3-4.7-84.7-20.6-84.7-91.9 0-20.3 7.3-36.9 19.2-49.9 -1.9-4.7-8.3-23.6 1.8-49.2 0 0 15.6-5 51.1 19.1 14.8-4.1 30.7-6.2 46.5-6.3 15.8 0.1 31.7 2.1 46.6 6.3 35.5-24 51.1-19.1 51.1-19.1 10.1 25.6 3.8 44.5 1.8 49.2 11.9 13 19.1 29.6 19.1 49.9 0 71.4-43.5 87.1-84.9 91.7 6.7 5.8 12.8 17.1 12.8 34.4 0 24.9 0 44.9 0 51 0 4.9 3 10.7 12.4 8.9 73.8-24.6 127-94.3 127-176.4C441.9 153.9 358.6 70.7 256 70.7z"/>
										</svg>
									</a>
								</li>
							</ul>
						</nav>
					</div><!-- /.o-grid__item -->


					<div class="o-grid__item  u-one-third  u-lap--two-thirds  u-palm--one-whole">
						<dl class="c-contact__details">
							<dt class="c-details__title">Give me a call on:</dt>
							<dd class="c-details__content  c-details__content--phone"><a href="tel:07415388501" class="">07415 388 501</a></dd>


							<dt class="c-details__title">Email me using:</dt>
							<dd class="c-details__content  c-details__content--email"><a href="mailto:say.hi@mangopear.co.uk" class="">say.hi@mangopear.co.uk</a></dd>
						</dl>
					</div><!-- /.o-grid__item -->


					<div class="o-grid__item  u-one-sixth  u-lap--one-third  u-palm--one-whole">
						<dl class="c-contact__details  c-contact__details--address">
							<dt class="c-details__title  ">Write to me at:</dt>
							<dd class="c-details__content--address">
								<p>
									Mangopear creative,<br>
									7 Schools Place,<br>
									3 Seaward Road,<br>
									Peartree,<br>
									Southampton,<br>
									SO19 2HA
								</p>
							</dd>
						</dl>
					</div><!-- /.o-grid__item -->
				</div><!-- /.o-grid -->
			</div><!-- /.o-container -->
		</footer><!-- /.o-panel -->





		<footer class="o-panel  o-panel--copyright">
			<div class="o-container">
				<div class="o-grid  u-clearfix">
					<div class="o-grid__item  u-one-half  u-portable--one-whole">
						<nav class="o-nav  o-nav--row  o-nav--footer">
							<ul class="o-nav__list  o-nav__list--footer">
								<li class="o-nav__item  o-nav__item--footer"><a href="/legal-stuff/terms-conditions/" class="o-nav__link  o-nav__link--footer">Terms &amp; conditions</a></li>
								<li class="o-nav__item  o-nav__item--footer"><a href="/legal-stuff/privacy-policy/" class="o-nav__link  o-nav__link--footer">Privacy Policy</a></li>
								<li class="o-nav__item  o-nav__item--footer"><a href="/legal-stuff/cookie-policy/" class="o-nav__link  o-nav__link--footer">Cookie Policy</a></li>
							</ul>
						</nav><!-- /.o-nav -->
					</div><!-- /.o-grid__item -->


					<div class="o-grid__item  u-one-half  u-portable--one-whole">
						<p class="c-copyright">&copy; 2015: Mangopear creative. All rights reserved.</p>
					</div><!-- /.o-grid__item -->
				</div><!-- /.o-grid -->
			</div><!-- /.o-container -->
		</footer><!-- /.o-panel -->





		<?php

			/**
			 * Call the WordPress footer function
			 */
			
			wp_footer();

		?>





		<!-- Web font loading -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					families: ['Roboto:300,400,700']
				}
			});
		</script>





		<!-- Google Analytics tracking code -->
		<script async defer>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','/wp-content/themes/mangopear/resources/js/analytics.js','ga');

			ga('create', 'UA-45542791-1', 'auto');
			ga('send', 'pageview');
		</script>
	</body>
</html>