<?php

	/**
	 * Mangopear framework function :: CONTENTS
	 *
	 * [1]	Enqueue our styles and scripts
	 * [2]	Register navigations for WordPress
	 * [3]	Add a stylesheet to wp-admin
	 * [4]	If the post/page uses the alternative header style, add a class to the body
	 * [5]	Custom image sizes for WordPress uploading
	 * [6]	Mangopear breadcrumbs
	 */
	




	/**
	 * [1]	Enqueue our styles and scripts
	 *
	 * 		[a]	@js 	global.min.js 		Our theme's default JS
	 * 		[b]	@css 	screen.css 			Compiled CSS file of all theme styles
	 *
	 * @since  0.1.0-alpha 
	 */
	
	function mangopear__enqueue_files()
	{
		// [a]
		wp_enqueue_script ('mangopear__global--scripts', get_stylesheet_directory_uri().'/resources/js/global.min.js', array('jquery'));

		// [b]
		wp_enqueue_style ('mangopear__global--styles', get_stylesheet_directory_uri().'/resources/css/compiled/screen.css');
	}


	// Tell WordPress to run these functions
	add_action ('wp_enqueue_scripts','mangopear__enqueue_files');





	/**
	 * [2]	Register navigations for WordPress
	 *
	 * @since  0.1.0-alpha
	 */
	
	function mangopear__register_menus() 
	{
		register_nav_menus(
			array(
				'main-nav' => __('Main Navigation')
			)
		);
	}

	add_action ('init', 'mangopear__register_menus');





	/**
	 * [3]	Add a stylesheet to wp-admin
	 *
	 * @since  0.1.0-alpha
	 */
	
	function mangopear__admin_stylesheet()
	{
		echo '<link rel="stylesheet" type="text/css" media="all" href="'.get_stylesheet_directory_uri().'/resources/css/compiled/admin.css">';
	}


	add_action ('admin_head', 'mangopear__admin_stylesheet');





	/**
	 * [4]	If the post/page uses the alternative header style, add a class to the body
	 *
	 * @since  0.2.0-alpha
	 */
	
	function mangopear__body_class__alternative_header($classes)
	{
		$mangopear__get_acf__alternative_header = get_field('main-header__background-image');

		if ($mangopear__get_acf__alternative_header) :
			$classes[] = 'body--alternative-header';
			return $classes;

		// Stop PHP from throwing a warning
		else :
			return $classes;

		endif;
	}

	add_action('body_class','mangopear__body_class__alternative_header');





	/**
	 * [5]	Custom image sizes for WordPress uploading
	 *
	 * @since 	0.4.0-alpha
	 */
	
	if ( function_exists( 'add_image_size' ) ) { 
	
		/**
		 * Homepage article size
		 */
		add_image_size(
			'homepage__article',
			400,
			222,
			true
		);
	}





	/**
	 * [6]	Mangopear breadcrumbs
	 */
	
	function mangopear_breadcrumbs() {
		global $post;


		echo '<nav class="c-breadcrumbs  o-nav  o-nav--row">';
			echo '<ul class="o-nav__list">';
				echo '<li class="o-nav__item"><a href="'.get_option('home').'" title="Home" class="o-nav__link">Home</a></li>';

				if (is_category()) :
					echo '<li class="o-nav__item"><a href="' . get_the_permalink(265) . '" class="o-nav__link">Blog</a></li>';
					echo '<li class="o-nav__item  o-nav__item--categories">';
						the_category(', ');
					echo '</li>';

				elseif (is_single()) :
					echo '<li class="o-nav__item"><a href="' . get_the_permalink(265) . '" class="o-nav__link">Blog</a></li>';
					if (has_category()) { echo '<li class="o-nav__item  o-nav__item--categories">'; the_category(', '); echo '</li>'; }
					echo '<li class="o-nav__item  o-nav__item--no-link">'.get_the_title().'</li>';

				elseif (is_page()) :
					if ($post->$post_parent) :
						$ancestors = get_post_ancestors($post->ID);

						foreach ($ancestors as $ancestor) :
							$output = '<li class="o-nav__item"><a href="'.get_the_permalink($ancestor).'" title="'.get_the_title($ancestor).'" class="o-nav__link">'.get_the_title($ancestor).'</a></li>' . $output;
						endforeach;

						echo $output;
						echo '<li class="o-nav__item  o-nav__item--no-link">'.get_the_title().'</li>';

					else :
						echo '<li class="o-nav__item  o-nav__item--no-link">'.get_the_title().'</li>';

					endif;

				elseif (is_tag()) :		single_tag_title();
				elseif (is_day()) :		echo '<li class="o-nav__item  o-nav__item--no-link">Posts made on: '.the_time('F js Y').'</li>';
				elseif (is_month()) :	echo '<li class="o-nav__item  o-nav__item--no-link">Posts made in: '.the_time('F Y').'</li>';
				elseif (is_year()) :	echo '<li class="o-nav__item  o-nav__item--no-link">Posts made in: '.the_time('Y').'</li>';
				elseif (is_author()) :	echo '<li class="o-nav__item  o-nav__item--no-link">Author archive</li>';
				elseif (is_search()) :	echo '<li class="o-nav__item  o-nav__item--no-link">Search results</li>';
				elseif (isset($_GET['paged']) && !empty($_GET['paged'])) :	echo '<li class="o-nav__item  o-nav__item--no-link">Archive</li>';
				endif;

			echo '</ul>';
		echo '</nav>';
	}





	/**
	 * [7]	Post formats
	 */
	
	add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));





	/**
	 * [8]	Dequeue SearchWP default styles
	 */
	
	
	add_action('wp_print_styles', 'my_remove_searchwp_live_search_theme_css', 99999);
	

	function my_remove_searchwp_live_search_theme_css() {
		wp_dequeue_style('searchwp-live-search');
		wp_deregister_style('searchwp-live-search');
	}





	/**
	 * [9]	Remove emoji support
	 */
	

	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');





	/**
	 * [10]	Remove jQuery migrate
	 */
	

	add_filter( 'wp_default_scripts', 'remove_jquery_migrate' );

	function remove_jquery_migrate(&$scripts) {
	    if(!is_admin()) {
	        $scripts->remove('jquery');
	        $scripts->add('jquery', false, array('jquery-core'), '1.10.2');
	    }
	}