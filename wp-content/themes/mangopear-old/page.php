<?php
	
	/**
	 * Get the header code
	 */
	get_header();

?>






	<header class="o-panel  o-panel--title">
		<div class="o-container  o-container--optimise-readability">
			<h1 class="c-page-title"><?php the_title(); ?></h1>
			<?php mangopear_breadcrumbs(); ?>
		</div>
	</header>





	<main class="o-panel">
		<div class="o-container  o-container--optimise-readability">
			<?php
		
				/**
				 * Default WordPress Loop call
				 */
				
				if (have_posts()) : 
					while (have_posts()) : the_post(); 

						the_content();

					endwhile;

				else :
					echo '<h2>Oh shucks, it looks like there isn\'t any content to be found here!</h2>';

				endif;

			?>
		</div><!-- /.o-container -->
	</main><!-- /.o-panel -->





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>