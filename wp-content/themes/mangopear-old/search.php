<?php
	
	/**
	 * Get the header code
	 */
	get_header();

?>





	<?php

				/**
				 * Get our posts
				 */
				
				if (have_posts()) :

			?>


				<section class="lister--blog">
					<div class="container">
						<h1 class="header--stylised">Search results: <?php echo $_GET['s']; ?></h1>
					</div>


					<ul class="grid  posts-list">
						<?php while (have_posts()) : the_post(); ?> 

							<li class="post  post--blog">
								<div class="typography--standard">
									<time datetime="<?php echo get_the_date('c'); ?>" class="post__date">
										<span class="date__day"><?php echo get_the_date('j'); ?></span>
										<span class="date__month"><?php echo get_the_date('M'); ?></span>
									</time>


									<article class="post__article">
										<h2 class="post__title">
											<a href="<?php the_permalink(); ?>" class="post__title__link"><?php the_title(); ?></a>
										</h2>


										<div class="post__excerpt"><?php the_excerpt(); ?></div>


										<a href="<?php the_permalink(); ?>" class="post__button  button--secondary">
											Read full article
											<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
												<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
											</svg>
										</a>
									</article>
								</div><!-- /.typography-\-standard -->
							</li>

						<?php endwhile; ?>
					</ul>
				</section><!-- /.container -->


			<?php else : ?>

				
				<section class="lister--blog  lister--no-posts  typography--standard">
					<h1 class="header--stylised">Search results: <?php echo $_GET['s']; ?></h1>

					<h2>Sorry, it looks like there aren't any results.</h2>
					<p>I'd recommend trying another search query...</p>
				</section>


			<?php endif; ?>





			<?php

				/**
				 * Let's get paginated
				 *
				 * [1]	Load up the global query var
				 * [2]	Create a ridiculously large number as a max for the  max number of posts
				 * [3]	Show the pagination
				 * [4]	Reset wp_query so that queries below work as expected
				 */

				/**
				 * [1]	Load up the global query var
				 */
			
				global $wp_query;


				/**
				 * [2]	Create a ridiculously large number as a max for the  max number of posts
				 */

				$big = 999999999;


				/**
				 * [3]	Show the pagination
				 */

				echo paginate_links( 
					array(
						'base'		=> str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
						'current'	=> max(1, get_query_var('paged')),
						'total'		=> $wp_query->max_num_pages,
						'format'	=> '?page=%#%',
						'type'		=> 'list',
						'end_size'	=> 3
					)
				);


				/**
				 * [4]	Reset wp_query so that queries below work as expected
				 */

				wp_reset_query();

			?>





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>