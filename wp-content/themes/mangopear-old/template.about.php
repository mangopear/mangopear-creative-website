<?php
	
	/**
	 * Template name: [About]
	 * 
	 * Get the header code
	 */
	
	get_header();

?>






	<header class="o-panel  o-panel--title">
		<div class="o-container  o-container--optimise-readability">
			<h1 class="c-page-title"><?php the_title(); ?></h1>
			<?php mangopear_breadcrumbs(); ?>
		</div>
	</header>





	<main class="o-panel">
		<div class="o-container  o-container--optimise-readability">
			<?php
		
				/**
				 * Default WordPress Loop call
				 */
				
				if (have_posts()) : 
					while (have_posts()) : the_post(); 

						the_content();

					endwhile;

				else :
					echo '<h2>Oh shucks, it looks like there isn\'t any content to be found here!</h2>';

				endif;

			?>
		</div><!-- /.o-container -->
	</main><!-- /.o-panel -->





	<footer class="o-panel  o-panel--hire-me">
		<div class="o-container">
			<div class="o-grid  o-grid--middle">
				<div class="o-grid__item  u-three-quarters  u-portable--one-whole  c-hire-me__content">
					<h2 class="c-hire-me__heading">Intrigued by what you have read so far?</h2>
					<p class="c-hire-me__text">Get in touch with me to see how I can make your branding; marketing and website do more for your business.</p>
				</div><!-- /.grid__item -->


				<div class="o-grid__item  u-one-quarter  u-portable--one-whole  c-hire-me__call-to-action">
					<a href="/contact/hire/" class="o-button  c-hire-me__button">
						<span class="o-button__text">Get in touch</span>
						<svg class="o-button__icon--right  o-icon--chevron-right" viewBox="0 0 16 16" width="20" height="20">
							<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
						</svg>
					</a><!-- /.hire-me__button -->
				</div><!-- /.o-grid__item -->
			</div><!-- /.o-grid -->
		</div><!-- /.o-container -->
	</footer><!-- /.o-panel -->





	<section class="o-panel  o-panel--blog  o-panel--testimonial">
		<div class="o-container">
			<div class="o-grid  o-grid--middle">
				<div class="o-grid__item  u-one-quarter  u-portable--one-whole  o-blog__wrapper--icon">
					<i class="o-blog__icon  o-blog__icon--testimonial">&rdquo;</i>
					<time datetime="" class="o-date__time  o-date__time--monthly">
						<span class="o-date__month">December</span>
						<span class="o-date__year">'14</span>
					</time>
				</div><!-- /.o-grid__item -->


				<div class="o-grid__item  u-three-quarters  u-portable--one-whole  o-blog__wrapper--content">
					<blockquote class="o-testimonial">
						<p>Andi North has been responsible for the administrative phased re-branding of Jeakins Coach Travel. This has involved front of house and back office publications.</p>
						<p>We are extremely satisfied with the consistently high standard of his product and the fact that Andi has grown to share the board’s values, ‘isms’ and expectations.</p>
						<p>Mark R Self, <strong>Jeakins Coach Travel</strong></p>
					</blockquote>
				</div><!-- /.grid__item -->
			</div><!-- /.o-grid -->
		</div><!-- /.o-container -->
	</section><!-- /.o-panel -->





	<?php get_template_part('resources/php/includes/availability'); ?>





	<section class="o-panel  o-panel--blog  o-panel--testimonial">
		<div class="o-container">
			<div class="o-grid  o-grid--middle">
				<div class="o-grid__item  u-one-quarter  u-portable--one-whole  o-blog__wrapper--icon">
					<i class="o-blog__icon  o-blog__icon--testimonial">&rdquo;</i>
					<time datetime="" class="o-date__time  o-date__time--monthly">
						<span class="o-date__month">July</span>
						<span class="o-date__year">'11</span>
					</time>
				</div><!-- /.o-grid__item -->


				<div class="o-grid__item  u-three-quarters  u-portable--one-whole  o-blog__wrapper--content">
					<blockquote class="o-testimonial">
						<p>Andi has been a tremendous asset helping us create and publish our website as well as being a consultant on various other issues, such a social media and graphics.</p>
						<p>He has been very helpful, delivering on time and working with us to help create what we want.</p>
						<p>Felicity Crabb, <strong>The Edge Project</strong></p>
					</blockquote>
				</div><!-- /.grid__item -->
			</div><!-- /.o-grid -->
		</div><!-- /.o-container -->
	</section><!-- /.o-panel -->






<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>