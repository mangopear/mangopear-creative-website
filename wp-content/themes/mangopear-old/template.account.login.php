<?php

	/**
	 * Template name: [Account] Login
	 */
	
	get_header();

?>






	<header class="o-panel  o-panel--title">
		<div class="o-container  o-container--optimise-readability">
			<h1 class="c-page-title"><?php the_title(); ?></h1>
			<?php mangopear_breadcrumbs(); ?>
		</div>
	</header>





	<main class="o-panel">
		<div class="o-container  o-container--optimise-readability">
			<form name="loginform" id="loginform" action="/wp-login.php" method="post" class="o-form  o-form--login">
				<fieldset>
					<input type="hidden" name="redirect_to" value="/account/dashboard/">
					

					<div class="o-form__field  o-form__field--username  o-form__field--text">
						<label for="user_login" class="o-form__label">Username: <span class="u-required">* <span class="u-invisible">(required)</span></span></label>
						<input type="text" id="user_login" name="log" value="" class="o-form__input  o-form__label--text  o-form__label--username">
					</div>
					

					<div class="o-form__field  o-form__field--password">
						<label for="user_pass" class="o-form__label">Password: <span class="u-required">* <span class="u-invisible">(required)</span></span></label>
						<input type="password" id="user_pass" name="pwd" value="" class="o-form__input  o-form__label--text  o-form__label--username">
					</div>
					

					<div class="o-form__field  o-form__field--remember  o-form__field--checkbox">
						<label for="rememberme" class="o-form__label">
							<input type="checkbox" id="rememberme" name="rememberme" value="forever">
							<span class="o-checkbox-label">Remember my details</span>
						</label>						
					</div>
					

					<div class="o-form__field  o-form__field--submit">
						<button class="o-button  o-button--secondary  o-button--submit" formnovalidate="formnovalidate">
							<span class="o-button__text">Log in now</span>
							<svg class="o-button__icon--right  o-icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
								<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
							</svg>
						</button>
					</div>


					<p class="o-form__forgotten">
						<a href="/wp-login.php?action=lostpassword">Forgotten your password?</a>
					</p>
				</fieldset>
			</form>
		</div><!-- /.o-container -->
	</main><!-- /.o-panel -->





	<?php get_footer(); ?>