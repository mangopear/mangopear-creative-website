<?php

	/**
	 * Template name: [Detail] Homepage
	 */
	
	get_header();

?>





	<main class="main  o-panel  o-panel--big  o-panel--home">
		<div class="o-container  o-container--optimise-readability">
			<h1 class="c-home__title">Design. Development. Consultancy.</h1>
			<p class="c-home__content  c-lede">Helping leading brands and independent companies break the mould. If you're looking for passion; dedication and attention to detail, look no further.</p>
			<!--<p class="c-home__content  c-lede">I'm Southampton-based; highly motivated; self employed and enjoy working with great companies. I feel passionately about the quality of work that I produce, always going the extra mile for my clients.</p>-->
			

			<div class="c-home__buttons">
				<a href="/what-i-do/" class="o-button  o-button--home  o-button--neutral">
					<span class="o-button__text">Find out more</span>
					<svg class="o-button__icon--right  o-icon--chevron-right" viewBox="0 0 16 16" width="20" height="20">
						<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
					</svg>
				</a><!-- /.o-button -->


				<a href="/contact/hire/" class="o-button  o-button--home  o-button--positive">
					<span class="o-button__text">Get in touch</span>
					<svg class="o-button__icon--right  o-icon--chevron-right" viewBox="0 0 16 16" width="20" height="20">
						<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
					</svg>
				</a><!-- /.o-button -->
			</div><!-- /.c-home__buttons -->
		</div><!-- /.container -->
	</main><!-- /.panel -->





	<!-- Web font loading -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,700']
			}
		});
	</script>





	<!-- Google Analytics tracking code -->
	<script async defer>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','/wp-content/themes/mangopear/resources/js/analytics.js','ga');

		ga('create', 'UA-45542791-1', 'auto');
		ga('send', 'pageview');
	</script>
</body>
</html>





<?php

	/**
	 * Get the footer code
	 */	
//	get_footer();

?>