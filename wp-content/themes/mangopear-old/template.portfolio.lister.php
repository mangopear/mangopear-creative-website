<?php
	
	/**
	 * Template name: Portfolio & Case Studies
	 * 
	 * Get the header code
	 */
	get_header();

?>





	<header class="o-panel  o-panel--title">
		<div class="o-container">
			<h1 class="c-page-title"><?php the_title(); ?></h1>
			<?php mangopear_breadcrumbs(); ?>
		</div>
	</header>





	<main class="o-panel  o-panel--primary">
		<div class="o-container">
			<div class="o-grid">
				<div class="o-grid__item  u-one-whole  u-lap--one-half  u-desk--one-third  lap--one-half  palm--one-whole">
					<article class="u-clearfix  c-portfolio-item  c-portfolio-item--jeakins  c-portfolio-item--has-link">
						<div class="c-portfolio-item__header">
							<h3 class="c-portfolio-item__heading"><a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="c-portfolio-item__header__link">Jeakins Coach Travel</a></h3>
							<p class="c-portfolio-item__content">A family-run coach operator based in the heart of Surrey.</p>
						</div>
						
						<a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="o-button  c-button--secondary  c-portfolio-item__button">
							<span class="button__text">Read more</span>
							<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
								<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
							</svg>
						</a>
					</article>
				</div><!-- /.grid__item -->


				<div class="o-grid__item  u-one-whole  u-lap--one-half  u-desk--one-third  lap--one-half  palm--one-whole">
					<article class="u-clearfix  c-portfolio-item  c-portfolio-item--gardbus  c-portfolio-item--has-link">
						<div class="c-portfolio-item__header">
							<h3 class="c-portfolio-item__heading"><a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="c-portfolio-item__header__link">Gardbus</a></h3>
							<p class="c-portfolio-item__content">A friendly, local operator running bus services in and around Ringwood.</p>
						</div>
						
						<a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="o-button  c-button--secondary  c-portfolio-item__button">
							<span class="button__text">Read more</span>
							<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
								<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
							</svg>
						</a>
					</article>
				</div><!-- /.grid__item -->


				<div class="o-grid__item  u-one-whole  u-lap--one-half  u-desk--one-third  lap--one-half  palm--one-whole">
					<article class="u-clearfix  c-portfolio-item  c-portfolio-item--silk  c-portfolio-item--has-link">
						<div class="c-portfolio-item__header">
							<h3 class="c-portfolio-item__heading"><a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="c-portfolio-item__header__link">Silk Web Toolkit</a></h3>
							<p class="c-portfolio-item__content">An open-source, pure HTML5 CMS built for security and performance.</p>
						</div>
						
						<a href="/wp-content/uploads/2014/10/Mangopear-creative-portfolio.pdf" class="o-button  c-button--secondary  c-portfolio-item__button">
							<span class="button__text">Read more</span>
							<svg class="button__icon--right  icon--chevron--right" viewBox="0 0 16 16" width="14" height="14">
								<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z"></path>
							</svg>
						</a>
					</article>
				</div><!-- /.grid__item -->
			</div><!-- /.grid -->
		</div><!-- /.container -->
	</main><!-- /.o-panel -->





	<footer class="o-panel  o-panel--secondary  o-panel--brands">
		<div class="o-container">
			<h2 class="o-panel__heading  c-panel__heading--portfolio">Other clients I have worked with...</h2>
			

			<div class="o-grid  o-grid--centre">
				<div class="o-grid__item  u-one-third  u-lap--one-half  u-palm--one-whole">
					<article class="u-clearfix  c-portfolio-item  c-portfolio-item--journee  c-portfolio-item--has-list">
						<div class="c-portfolio-item__header">
							<h3 class="c-portfolio-item__heading">Journee</h3>
							<p class="c-portfolio-item__content">A top secret product that will be launching very soon!</p>
						</div>
						
						<ul class="c-portfolio-item__list">
							<li>Branding</li>
							<li>Digital design</li>
							<li>Website development</li>
							<li>Systems integration</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->


				<div class="o-grid__item  u-one-third  u-lap--one-half  u-palm--one-whole">
					<article class="u-clearfix  c-portfolio-item  c-portfolio-item--devpod  c-portfolio-item--has-list">
						<div class="c-portfolio-item__header">
							<h3 class="c-portfolio-item__heading">DevPod</h3>
							<p class="c-portfolio-item__content">The sexiest pod of developers on the world wide web!*</p>
						</div>
						
						<ul class="c-portfolio-item__list">
							<li>Member</li>
							<li>Branding</li>
							<li>Website development</li>
						</ul>


						<p class="c-portfolio-item__note">* Not entirely the truth</p>
					</article>
				</div><!-- /.grid__item -->


				<div class="o-grid__item  u-one-third  u-lap--one-half  u-palm--one-whole">
					<article class="u-clearfix  c-portfolio-item  c-portfolio-item--theuvg  c-portfolio-item--has-list">
						<div class="c-portfolio-item__header">
							<h3 class="c-portfolio-item__heading">The UVG</h3>
							<p class="c-portfolio-item__content">Bringing the joy of music to South Hampshire.</p>
						</div>
						
						<ul class="c-portfolio-item__list">
							<li>Website development</li>
							<li>Digital consultancy</li>
							<li>Digital design</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->


				<div class="o-grid__item  u-one-third  u-lap--one-half  u-palm--one-whole">
					<article class="u-clearfix  c-portfolio-item  c-portfolio-item--edge  c-portfolio-item--has-list">
						<div class="c-portfolio-item__header">
							<h3 class="c-portfolio-item__heading">The EDGE Project</h3>
							<p class="c-portfolio-item__content">Events run by young people for young people.</p>
						</div>
						
						<ul class="c-portfolio-item__list">
							<li>Member</li>
							<li>Website development</li>
							<li>Digital consulting</li>
							<li>Training</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->


				<div class="o-grid__item  u-one-third  u-lap--one-half  u-palm--one-whole">
					<article class="u-clearfix  c-portfolio-item  c-portfolio-item--winnallca  c-portfolio-item--has-list">
						<div class="c-portfolio-item__header">
							<h3 class="c-portfolio-item__heading">Winnall CA</h3>
							<p class="c-portfolio-item__content">At the heart of the community in Winnall, Winchester.</p>
						</div>
						
						<ul class="c-portfolio-item__list">
							<li>Branding</li>
							<li>Digital design</li>
							<li>Website consultancy</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->


				<div class="o-grid__item  u-one-third  u-lap--one-half  u-palm--one-whole">
					<article class="u-clearfix  c-portfolio-item  c-portfolio-item--youthview  c-portfolio-item--has-list">
						<div class="c-portfolio-item__header">
							<h3 class="c-portfolio-item__heading">Youthview</h3>
							<p class="c-portfolio-item__content">Your view to youth services in Winnall and Highcliffe.</p>
						</div>
						
						<ul class="c-portfolio-item__list">
							<li>Branding</li>
							<li>Website development</li>
							<li>Digital consulting</li>
						</ul>
					</article>
				</div><!-- /.grid__item -->
			</div><!-- /.grid -->
		</div><!-- /.container -->
	</footer><!-- /.panel -->





<?php get_footer(); ?>