<?php
	
	/**
	 * Template name: What I do
	 * 
	 * Get the header code
	 */
	
	get_header();

?>






	<header class="o-panel  o-panel--title">
		<div class="o-container  o-container--optimise-readability">
			<h1 class="c-page-title">What I do and why I love it…</h1>
			<?php mangopear_breadcrumbs(); ?>
		</div>
	</header>





	<main class="o-panel">
		<div class="o-container  o-container--optimise-readability">
			<h2>Hi. I'm Andi.</h2>
			<p class="c-lead">A highly motivated designer, developer and consultant. I'm self employed and enjoy working with some great companies, ranging from independent bus companies to high-end suppliers to online startups.</p>
			<p>I provide consultancy to organisations on how they can get the most out of their website by understanding where it is currently letting them down and advising them on how to address those issues. I also design and build websites for small and medium sized organisations.</p>
			<p>I have three years of agency experience building websites for leading retailers and strategists in the UK, whilst also gaining experience of working with smaller, local companies and charities.</p>
			<p>I don't just work online though, I have experience designing for print too - including branding; timetable design and brochure production. Look at my portfolio to <a href="/portfolio/" title="View my portfolio" class="button--tertiary">see more of what I have done</a>.</p>
			<p>I feel passionately about the quality of work that I produce, always going the extra mile for my clients. It may sometimes takes me a little longer than most to create the work I do as I am picky about the little details, always ensuring things are done right.</p>
			<p>I'm available for hire and looking to work with organisations that are open to change and seeking to improve their professional appearance.</p>
			<p>I'm a freelancer because I thrive on the variety of work I do and enjoy meeting interesting new companies and people; working on quality projects with clients that value the worth of great design and realise the power it holds within their business.</p>
			<p class="c-lead">If that sounds like you and your project, <a href="/contact/hire/" title="Hire me" class="button--tertiary">get in touch</a> today!</p>
		</div><!-- /.o-container -->
	</main><!-- /.o-panel -->





	<footer class="o-panel  o-panel--hire-me">
		<div class="o-container">
			<div class="o-grid  o-grid--middle">
				<div class="o-grid__item  u-three-quarters  u-portable--one-whole  c-hire-me__content">
					<h2 class="c-hire-me__heading">Intrigued by what you have read so far?</h2>
					<p class="c-hire-me__text">Get in touch with me to see how I can make your branding; marketing and website do more for your business.</p>
				</div><!-- /.grid__item -->


				<div class="o-grid__item  u-one-quarter  u-portable--one-whole  c-hire-me__call-to-action">
					<a href="/contact/hire/" class="o-button  c-hire-me__button">
						<span class="o-button__text">Get in touch</span>
						<svg class="o-button__icon--right  o-icon--chevron-right" viewBox="0 0 16 16" width="20" height="20">
							<path fill="currentColor" d="M.156 0l.125.125 7.906 7.875-8 8h5.625l6.594-6.594 1.438-1.406-1.438-1.406-6.563-6.594h-5.688z" />
						</svg>
					</a><!-- /.hire-me__button -->
				</div><!-- /.o-grid__item -->
			</div><!-- /.o-grid -->
		</div><!-- /.o-container -->
	</footer><!-- /.o-panel -->





	<section class="o-panel  o-panel--blog  o-panel--testimonial">
		<div class="o-container">
			<div class="o-grid  o-grid--middle">
				<div class="o-grid__item  u-one-quarter  u-portable--one-whole  o-blog__wrapper--icon">
					<i class="o-blog__icon  o-blog__icon--testimonial">&rdquo;</i>
					<time datetime="" class="o-date__time  o-date__time--monthly">
						<span class="o-date__month">December</span>
						<span class="o-date__year">'14</span>
					</time>
				</div><!-- /.o-grid__item -->


				<div class="o-grid__item  u-three-quarters  u-portable--one-whole  o-blog__wrapper--content">
					<blockquote class="o-testimonial">
						<p>Andi North has been responsible for the administrative phased re-branding of Jeakins Coach Travel. This has involved front of house and back office publications.</p>
						<p>We are extremely satisfied with the consistently high standard of his product and the fact that Andi has grown to share the board’s values, ‘isms’ and expectations.</p>
						<p>Mark R Self, <strong>Jeakins Coach Travel</strong></p>
					</blockquote>
				</div><!-- /.grid__item -->
			</div><!-- /.o-grid -->
		</div><!-- /.o-container -->
	</section><!-- /.o-panel -->





	<?php get_template_part('resources/php/includes/availability'); ?>





	<section class="o-panel  o-panel--blog  o-panel--testimonial">
		<div class="o-container">
			<div class="o-grid  o-grid--middle">
				<div class="o-grid__item  u-one-quarter  u-portable--one-whole  o-blog__wrapper--icon">
					<i class="o-blog__icon  o-blog__icon--testimonial">&rdquo;</i>
					<time datetime="" class="o-date__time  o-date__time--monthly">
						<span class="o-date__month">July</span>
						<span class="o-date__year">'11</span>
					</time>
				</div><!-- /.o-grid__item -->


				<div class="o-grid__item  u-three-quarters  u-portable--one-whole  o-blog__wrapper--content">
					<blockquote class="o-testimonial">
						<p>Andi has been a tremendous asset helping us create and publish our website as well as being a consultant on various other issues, such a social media and graphics.</p>
						<p>He has been very helpful, delivering on time and working with us to help create what we want.</p>
						<p>Felicity Crabb, <strong>The Edge Project</strong></p>
					</blockquote>
				</div><!-- /.grid__item -->
			</div><!-- /.o-grid -->
		</div><!-- /.o-container -->
	</section><!-- /.o-panel -->






<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>