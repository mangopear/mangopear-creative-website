# Contributing

This is a closed source project. To contribute, you must be authorised by Hildon Limited or Mangopear creative.

## Issues

- Report issues or feature requests on [Trello](https://trello.com/b/yl09jtOq/hildon-2015-website-revamp-project-board).
- If reporting a bug, please add as much detail as possible (server environment, expected behaviour, actual behaviour, code examples, etc)

## Pull requests

- Create a new topic branch for each change you make.
- Create a test case if you are fixing a bug or implementing an important feature.
- Make sure the theme runs successfully before submitting your pull request.
- Open a Pull Request with a clear title and description.
