<?php

	/**
	 * Core template: [Archive] Portfolio
	 *
	 * @category 	Additional WordPress template files
	 * @package  	mangopear
	 * @author  	Andi North <andi@mangopear.co.uk>
	 * @copyright  	2015 Mangopear creative
	 * @license   	GNU General Public License <http://opensource.org/licenses/gpl-license.php>
	 * @version  	3.0.0
	 * @link 		https://mangopear.co.uk/
	 * @since   	2.0.0
	 */
	

	get_header();


	/**
	 * Output page title
	 *
	 * @see /themes/mangopear/functions/source/mangopear/mangopear.output.page-title.php
	 */
	
	mangopear_output_page_title($show_title = true, $show_breadcrumb = true, $title_content = 'Our work');
	
?>


	<main class="o-panel">
		<div class="o-container">
			<div class="o-container  o-container--optimise-readability  o-container--align-left">
				<p class="c-lede" style="max-width: 500px;">We’re really rather proud of our work here at Mangopear, below is a sample of the work we're proudest of.</p>
			</div><!-- /.o-container -->
		</div><!-- /.o-container -->
	</main><!-- /.o-panel -->





	<?php mangopear_panel_portfolio($location = 'archive-portfolio', $title = false); ?>





<?php get_footer(); ?>