<?php

	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) :
		die('You can\'t access this file directly. You naughty little thing, you!');

		if (! empty($post->$post_password)) :
			if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) :
				echo '<p>This post is password protected. Enter the password to view comments.</p>';
				return;
			endif;
		endif;
	endif;

?>


	<div class="o-grid">
		<div class="o-grid__item  u-three-quarters  u-lap--two-thirds  u-palm--one-whole">
			<?php if ($comments) : $i = 1; ?>
				<ol class="list--comments">
					<?php foreach ($comments as $comment) : ?>
						<li class="comment  comment--<?php comment_id(); ?>" id="blog-comment--<?php comment_id(); ?>">
							<header class="comment__header">
								<cite><?php comment_author_link(); ?></cite>
								<span class="comment__integer"><?php echo $i; $i++; ?></span>
							</header>


							<article class="comment__content">
								<?php if ($comment->comment_approved == '0') : echo 'Your comment is awaiting moderation'; ?>
								<?php else : comment_text(); ?>
								<?php endif; ?>
							</article>


							<footer class="comment__footer">
								<a href="#comment-<?php comment_ID() ?>"><?php comment_date('F jS, Y') ?> at <?php comment_time() ?></a>
							</footer>
						</li>
					<?php endforeach; ?>
				</ol>


				<?php comment_form(); ?>


			<?php

				else :
					if ($post->comment_status == 'open') : echo '<p><strong>Start the conversation - <em>be the first to comment</em>.</strong></p>'; comment_form();
					else : echo '<p><strong>Comments are currently closed.</strong></p>';
					endif;
				endif;

			?>
		</div><!-- /.o-grid__item -->


		<div class="o-grid__item  u-one-quarter  u-lap--one-third  u-palm--one-whole">
			<a href="#leave-a-comment" class="o-button  o-button--primary">Leave a comment</a>
		</div><!-- /.o-grid__item -->
	</div><!-- /.o-grid -->