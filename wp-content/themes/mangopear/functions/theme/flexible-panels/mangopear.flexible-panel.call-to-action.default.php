<?php
	
	/**
	 * Flexible content panel: Call to action - Default version
	 */
	
	function mangopear_panel_cta_default() { ?>


		<footer class="o-panel  o-panel--hire-me">
			<div class="o-container">
				<div class="o-grid  o-grid--middle">
					<div class="o-grid__item  u-three-quarters  u-portable--one-whole  c-hire-me__content">
						<h2 class="c-hire-me__heading">Get your free consultation now...</h2>
						<p class="c-hire-me__text">It's rather simple, all you need to do is get in touch with us and we'll set up a meeting with you and your team.</p>
					</div><!-- /.grid__item -->


					<div class="o-grid__item  u-one-quarter  u-portable--one-whole  c-hire-me__call-to-action">
						<a href="/contact/hire/" class="o-button  o-button--primary  o-button--call-to-action">
							<span class="o-button__text">Get in touch</span>
							<svg class="o-button__icon--right  o-icon--chevron-right" viewBox="0 0 36 36" width="24" height="24">
								<rect fill="currentColor" y="16.5" width="31.3" height="3"/>
								<polygon fill="currentColor" points="19.2,31.9 17.3,29.6 31.3,18 17.3,6.4 19.2,4.1 36,18 "/>
							</svg>
						</a><!-- /.hire-me__button -->
					</div><!-- /.o-grid__item -->
				</div><!-- /.o-grid -->
			</div><!-- /.o-container -->
		</footer><!-- /.o-panel -->


	<?php } ?>