<?php
	
	/**
	 * Flexible content panel: Services we offer
	 */
	
	function mangopear_panel_services($show_button = false) {

?>


		<section class="o-panel  o-panel--services-we-offer">
			<div class="o-container">
				<h2 class="o-panel__heading  c-panel__heading--portfolio">Services we offer</h2>


				<div class="o-grid  o-grid--center  o-grid--middle">
					<div class="o-grid__item  u-one-third  u-lap--one-half  u-palm--one-whole">
						<article class="c-our-service  c-our-service--consultancy">
							<?php /* <a href="/what-we-do/consultation/" class="c-our-service__link"> */ ?>
								<div class="c-our-services__graphic">
									<div class="c-our-services__graphic-inner">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 200" width="300" height="200" xml:space="preserve" class="u-consultancy-graphic">
											<g class="u-consultancy-graphic__group">
												<path   fill="#1cb6c3" class="u-consultancy-graphic__bubble" d="M255,79.3h-23.2V100l-20.9-20.7H105c-2.8,0-5-2.3-5-5v-40c0-2.8,2.3-5,5-5h150c2.8,0,5,2.3,5,5v40 C260,77.1,257.8,79.3,255,79.3z"/>
												<circle fill="#FFFFFF" class="u-consultancy-graphic__circle" cx="152" cy="54.3" r="7.5"/>
												<circle fill="#FFFFFF" class="u-consultancy-graphic__circle" cx="180" cy="54.3" r="7.5"/>
												<circle fill="#FFFFFF" class="u-consultancy-graphic__circle" cx="208" cy="54.3" r="7.5"/>
											</g>
											<g class="u-consultancy-graphic__group">
												<path   fill="#1cb6c3" class="u-consultancy-graphic__bubble" d="M45,150h23.2v20.7L89.1,150H195c2.8,0,5-2.3,5-5v-40c0-2.8-2.3-5-5-5H45c-2.8,0-5,2.3-5,5v40 C40,147.8,42.3,150,45,150z"/>
												<circle fill="#FFFFFF" class="u-consultancy-graphic__circle" cx="92" cy="125" r="7.5"/>
												<circle fill="#FFFFFF" class="u-consultancy-graphic__circle" cx="120" cy="125" r="7.5"/>
												<circle fill="#FFFFFF" class="u-consultancy-graphic__circle" cx="148" cy="125" r="7.5"/>
											</g>
										</svg>
									</div><!-- /.c-our-services__graphic-inner -->
								</div><!-- /.c-our-services__graphic -->

								<h3 class="c-our-services__title">Consultation</h3>
							<?php /* </a> */ ?>
						</article>
					</div><!-- /.o-grid__item -->





					<div class="o-grid__item  u-one-third  u-lap--one-half  u-palm--one-whole">
						<article class="c-our-service  c-our-service--design">
							<?php /* <a href="/what-we-do/design/" class="c-our-service__link"> */ ?>
								<div class="c-our-services__graphic">
									<div class="c-our-services__graphic-inner">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 200" width="300" height="200" xml:space="preserve" class="u-design-graphic">
											<path class="u-design-graphic__solid"  style="fill: none;" d="M100.1,121.4l0.6-44.1c0.1-4.1,2.3-7.8,5.8-9.8L145,46c3.6-2,7.9-1.9,11.4,0.2l37.9,22.5 c3.5,2.1,5.6,5.9,5.6,9.9l-0.6,44.1c-0.1,4.1-2.3,7.8-5.8,9.8L155,154c-3.6,2-7.9,1.9-11.4-0.2l-37.9-22.5 C102.2,129.2,100.1,125.5,100.1,121.4z"/>
											<path class="u-design-graphic__stroke" style="fill: none; stroke: #004D5D; stroke-width: 6; stroke-miterlimit: 10;" d="M100.2,121.4l0.6-44.1c0.1-4.1,2.3-7.8,5.8-9.8L145.1,46c3.6-2,7.9-1.9,11.4,0.2l37.9,22.5 c3.5,2.1,5.6,5.9,5.6,9.9l-0.6,44.1c-0.1,4.1-2.3,7.8-5.8,9.8L155.1,154c-3.6,2-7.9,1.9-11.4-0.2l-37.9-22.5 C102.3,129.2,100.2,125.5,100.2,121.4z"/>
											<path class="u-design-graphic__m"      style="fill: none;" d="M178.1,97.5v19.3h-11.3V99.3c0-4.8-2.4-7.1-6-7.1c-1.7,0-3.5,0.8-5.1,2.8c0.1,0.8,0.1,1.7,0.1,2.5v19.3h-11.3 V99.3c0-4.8-2.4-7.1-6-7.1c-1.7,0-3.4,0.8-5,2.9v21.6h-11.3V83.1h11.3V85c2.2-1.9,4.6-3,8.3-3c4.2,0,7.9,1.6,10.4,4.6 c3.5-3.2,6.9-4.6,12-4.6C172.1,82,178.1,87.9,178.1,97.5z"/>
										</svg>
									</div><!-- /.c-our-services__graphic-inner -->
								</div><!-- /.c-our-services__graphic -->

								<h3 class="c-our-services__title">Design</h3>
							<?php /* </a> */ ?>
						</article>
					</div><!-- /.o-grid__item -->





					<div class="o-grid__item  u-one-third  u-lap--one-half  u-palm--one-whole">
						<article class="c-our-service  c-our-service--development">
							<?php /* <a href="/what-we-do/development/" class="c-our-service__link"> */ ?>
								<div class="c-our-services__graphic">
									<div class="c-our-services__graphic-inner">
										<pre class="u-development-graphic"><?php 
											
											$output  = '<span class="u-development-graphic__static">&gt;|';
											$output .= '</span>';


											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">h</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">e</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">l</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">l</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">o</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">_</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">w</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">o</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">r</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">l</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">d</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">(</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">)</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">;</span>';


											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue"><br></span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue"> </span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue"> </span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue"> </span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue"> </span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue"> </span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue"> </span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue"> </span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue"><br></span>';


											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">w</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">e</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">_</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">b</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">u</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">i</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">l</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">d</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">_</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">w</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">e</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">b</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">s</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">i</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">t</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">e</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--blue">s</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">(</span>';


											$output .= '<br/>';


											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">    $</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">q</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">u</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">a</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">l</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">i</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">t</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">y</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">: </span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">\'</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">o</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">u</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">t</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">s</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">t</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">a</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">n</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">d</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">i</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">n</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">g</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--orange">\'</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">;</span>';


											$output .= '<br/>';


											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">)</span>';
											$output .= '<span class="u-development-graphic__reveal  u-development-graphic__reveal--white">;</span>';


											echo $output;
										?></pre>
									</div><!-- /.c-our-services__graphic-inner -->
								</div><!-- /.c-our-services__graphic -->

								<h3 class="c-our-services__title">Development</h3>
							<?php /* </a> */ ?>
						</article>
					</div><!-- /.o-grid__item -->
				</div><!-- /.o-grid -->





				<?php if ($show_button != false) : ?>
					<div class="o-panel__button-wrapper">
						<a href="/what-we-do/" class="o-button  o-button--secondary  o-panel__button">
							<span class="o-button__text">Read more about what we do</span>
							<svg class="o-button__icon--right  o-icon--chevron-right" viewBox="0 0 36 36" width="24" height="24">
								<rect fill="currentColor" y="16.5" width="31.3" height="3"></rect>
								<polygon fill="currentColor" points="19.2,31.9 17.3,29.6 31.3,18 17.3,6.4 19.2,4.1 36,18 "></polygon>
							</svg>
						</a>
					</div>
				<?php endif; ?>
			</div><!-- /.o-container -->
		</section><!-- /.o  o-panel -->


	<?php } ?>