<?php

	/**
	 * Template include: [Portfolio] UVG: UVG landing
	 *
	 * @category 	Template include
	 * @package  	mangopear
	 * @author  	Andi North <andi@mangopear.co.uk>
	 * @copyright  	2016 Mangopear creative
	 * @license   	GNU General Public License <http://opensource.org/licenses/gpl-license.php>
	 * @version  	3.0.0
	 * @link 		https://mangopear.co.uk/
	 * @since   	2.0.0
	 */
	
?>


	<header class="u-clearfix  o-header--portfolio  o-header--portfolio--uvg">
		<div class="c-portfolio-header__gradient"></div>
		

		<div class="c-portfolio-header__content">
			<h1 class="c-portfolio-header__title"><?php the_title(); ?></h1>
			<h2 class="c-portfolio-header__title--sub">A Mangopear creative project</h2>
			<h3 class="c-portfolio-header__descriptor"><?php the_excerpt(); ?></h3>
		</div><!-- /.c-portfolio-header__content -->
	</header>





	<main>
		<article class="c-portfolio-article  c-portfolio-article--banded">
			<div class="o-container">
				<div class="o-container  o-container--optimise-readability">
					<?php the_content(); ?>
				</div><!-- /.o-container -->
			</div><!-- /.o-container -->
		</article><!-- /.c-portfolio-article -->
	</main>