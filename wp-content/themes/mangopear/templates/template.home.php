<?php

	/**
	 * Template name: [Detail] Homepage
	 */
	
	get_header();

?>





	<main class="main  o-panel  o-panel--gutterless">
		<div class="parallax-window  u-clearfix" data-parallax="scroll" data-bleed="10" data-speed="0.75" data-natural-width="1920" data-natural-height="1040" data-image-src="/wp-content/themes/mangopear/resources/images/home/panel--top.jpg" style="min-height: 900px;">
			<div class="o-container  o-container--optimise-readability  o-container--home  u-clearfix">
				<h1 class="c-home__title">Are you using the right tools for the job?</h1>
				<p class="c-home__content  c-lede">With a free consultation from Mangopear creative, we can work together to ensure your marketing tools are working for you.</p>
				

				<div class="c-home__buttons">
					<a href="/what-we-do/consultation/" class="o-button  o-button--primary">
						<span class="o-button__text">Get a free consultation</span>
						<svg class="o-button__icon--right  o-icon--chevron-right" viewBox="0 0 36 36" width="24" height="24">
							<rect fill="currentColor" y="16.5" width="31.3" height="3"/>
							<polygon fill="currentColor" points="19.2,31.9 17.3,29.6 31.3,18 17.3,6.4 19.2,4.1 36,18 "/>
						</svg>
					</a><!-- /.hire-me__button -->


					<span class="c-home__buttons-or">-&nbsp; or &nbsp;-</span>


					<a href="/what-we-do/" class="o-button  o-button--secondary  o-panel__button">
						<span class="o-button__text">Read about what we do</span>
						<svg class="o-button__icon--right  o-icon--chevron-right" viewBox="0 0 36 36" width="24" height="24">
							<rect fill="currentColor" y="16.5" width="31.3" height="3"/>
							<polygon fill="currentColor" points="19.2,31.9 17.3,29.6 31.3,18 17.3,6.4 19.2,4.1 36,18 "/>
						</svg>
					</a>
				</div><!-- /.c-home__buttons -->
			</div><!-- /.container -->
		</div><!-- /.parallax-window -->
	</main><!-- /.panel -->





	<?php mangopear_panel_services($show_button = true); ?>





	<!-- Web font loading -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,700']
			}
		});
	</script>





	<!-- Google Analytics tracking code -->
	<script async defer>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','/wp-content/themes/mangopear/resources/js/analytics.js','ga');

		ga('create', 'UA-45542791-1', 'auto');
		ga('send', 'pageview');
	</script>
</body>
</html>





<?php

	/**
	 * Get the footer code
	 */	
	get_footer();

?>